# Outils specifiques au serveur web (dolibarr / administratif)

## Génération automatique des factures

Le système s'appuie sur des factures modèles qui vont donc être automatiquement générées à la date de facturation.
Le problème est de les envoyer ensuite par mail aux adhérents ... voir même il faudrait dans le cas "idéal" les avertir "avant" qu'ils vont avoir une facture dans 60 jours, 30 jours, 1 semaine ... leur laissant le temps d'annuler leur abonnement s'ils le souhaitent ... mais pour l'instant le choix de l'association est de dire qu'on annule des factures à postériori et qu'on trouve toujours une solution humaine avec les adhérents.

L'adresse mail à laquelle on envoie la facture est l'adresse configurée par l'adhérent sur sa fiche mais parfois il a une adresse qu'il ne consulte "plus" vu qu'on héberge son adresse sur nos serveurs ... c'est son adresse de secours qu'il a configuré sur son compte et comme on a son adresse hébergée dans le PDF de la facture on utilise la commande pdfgrep pour récupérer cette info dans le pdf de la facture.

L'envoi automatique des mails avec factures jointes est lancé par /etc/cron.daily/soo2facture

## Relance des factures impayées

Lancé par /etc/cron.monthly/soo2facturesImpayees