<?php
/* Copyright (C) 2017      Eric Seigne	<eric.seigne@cap-rel.fr>
   *
   * This program is free software; you can redistribute it and/or modify
   * it under the terms of the GNU General Public License as published by
   * the Free Software Foundation; either version 3 of the License, or
   * (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   * GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with this program. If not, see <http://www.gnu.org/licenses/>.
   */

/**
 *   	\file       htdocs/sudouests/admin/sudouest.php
 *		\ingroup    sudouest
 *		\brief      Page to setup the module Foundation
 */

require '../../main.inc.php';
require_once DOL_DOCUMENT_ROOT . '/core/lib/admin.lib.php';

$langs->load("admin");

if (!$user->admin) accessforbidden();


$type = array('yesno', 'texte', 'chaine');

$action = GETPOST('action', 'alpha');


/*
 * Actions
 */

// Action mise a jour ou ajout d'une constante
if ($action == 'update' || $action == 'add') {
    $constname = GETPOST('constname', 'alpha');
    $constvalue = (GETPOST('constvalue_' . $constname) ? GETPOST('constvalue_' . $constname) : GETPOST('constvalue'));

    if (($constname == 'SUDOUEST_CARD_TYPE' || $constname == 'SUDOUEST_ETIQUETTE_TYPE' || $constname == 'SUDOUEST_PRODUCT_ID_FOR_SUBSCRIPTIONS') && $constvalue == -1) $constvalue = '';
    if ($constname == 'SUDOUEST_LOGIN_NOT_REQUIRED') // Invert choice
    {
        if ($constvalue) $constvalue = 0;
        else $constvalue = 1;
    }

    $consttype = GETPOST('consttype', 'alpha');
    $constnote = GETPOST('constnote');
    $res = dolibarr_set_const($db, $constname, $constvalue, $type[$consttype], 0, $constnote, $conf->entity);

    if (!$res > 0) $error++;

    if (!$error) {
        setEventMessages($langs->trans("SetupSaved"), null, 'mesgs');
    } else {
        setEventMessages($langs->trans("Error"), null, 'errors');
    }
}

// Action activation d'un sous module du module sudouest
if ($action == 'set') {
    $result = dolibarr_set_const($db, GETPOST('name', 'alpha'), GETPOST('value'), '', 0, '', $conf->entity);
    if ($result < 0) {
        print $db->error();
    }
}

// Action desactivation d'un sous module du module sudouest
if ($action == 'unset') {
    $result = dolibarr_del_const($db, GETPOST('name', 'alpha'), $conf->entity);
    if ($result < 0) {
        print $db->error();
    }
}



/*
 * View
 */

$form = new Form($db);

$help_url = 'EN:Module_Foundations|FR:Module_Adh&eacute;rents|ES:M&oacute;dulo_Miembros';

llxHeader('', $langs->trans("SudOuestSetup"), $help_url);


$linkback = '<a href="' . DOL_URL_ROOT . '/admin/modules.php">' . $langs->trans("BackToModuleList") . '</a>';
print load_fiche_titre($langs->trans("SudOuestSetup"), $linkback, 'title_setup');

dol_fiche_head($head, 'general', $langs->trans("Sudouest"), 0, 'user');


/*
 * Edition info modele document
 */
$constantes = array(
    'SOO2_MX_IN',
    'SOO2_URI_MAILINGLISTES',
    'SOO2_MODOBOA_API_TOKEN',
    'SOO2_MODOBOA_API_URL',
    'SOO2_MAIL_DOMAINS',
    'SOO2_MAIL_ERRORSTO',
    'SOO2_MAIL_ABUSE',
    'SOO2_MAIL_POSTMASTER',
    'SOO2_MAIL_WEB_URL'
);

print load_fiche_titre($langs->trans("Configuration"), '', '');

form_constantes($constantes);

print '<br>';


dol_fiche_end();


llxFooter();

$db->close();
