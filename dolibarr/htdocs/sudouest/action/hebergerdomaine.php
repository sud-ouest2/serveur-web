<?php
/*
 * Copyright (C) 2017      Eric Seigne        <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 *  \file       htdocs/sudouest/action/hebergerdomaine.php
 *  \ingroup    sudouest
 *  \brief      Home page of sudouest
 */

require '../../main.inc.php';
require 'common.php';

$action = GETPOST('action', 'alpha');

if (!$user->rights->sudouest->actions->lire)
	accessforbidden();

/*
 * View
 */
function print_form_ouvertureDomaineHebergeSOO()
{
	global $db, $conf, $langs;

	print '<form action="' . $_SERVER["PHP_SELF"] . '" method="POST" name="ouvrirHebergement">' . "\n";
	print '<input type="hidden" name="token" value="' . $_SESSION['newtoken'] . '" / >';
	print '<input type="hidden" name="entity" value="' . $entity . '" />';
	print '<input type="hidden" name="action" value="ouvrirHebergement" />';

	print "    <div style=\"text-align: left; background: #eee; padding: 5px; margin: 5px;\">
	<div>
          <br/>&nbsp;&nbsp;<b>Vous voulez faire héberger vos boites mails sur votre propre nom de domaine</b>\n";
	print estimationTarifs();
	print "	     <div style=\"border-left: 3px dotted #000000; margin-left: 10px; margin-bottom: 15px;\">
		<p class=\"infos\" style=\"margin: 8px 0 0 15px;\">
			Exemple: Vous possédez le nom de domaine <b>hiphiphiphoura.fr</b> et vous souhaitez créer des comptes mails <b>abc@hiphiphiphoura.fr</b> ... <b>xyz@hiphiphiphoura.fr</b> pour les membres de votre Association, pour vous ou pour votre entourage.
		</p>
	      	<p style=\"margin:8px 0 0 15px;\">
		        Vous possédez votre nom de domaine : Lequel ? (par exemple mondomaine.fr) <input class=\"forms\" type=\"text\" name=\"votreDomaineMail\" size=\"15\" maxlength=\"35\" value=\"\" />
		</p>
	     </div>

	     <div>
	      	<p style=\"margin:30px 0 0 0;\"><b>&#9670; De quel espace de stockage avez vous besoin ?</b> \n";
	//On cherche tous les services dont le nom commence par domainmail et on fait une liste ...
	//erics2
	$paymenttermstatic = new PaymentTerm($db);


	$sql = "SELECT * FROM " . MAIN_DB_PREFIX . "product WHERE ref LIKE 'domainmail%' ORDER BY ref ASC";
	$res = $db->query($sql);
	if ($res) {
		print "<select class=\"flat maxwidth200\" name=\"domainmailQuota\" id=\"space_id\">\n";
		$record = array();
		while ($record = $db->fetch_array($res)) {
			//astuce, on a besoin du quota pour creer la boite, la nomenclature des services est du genre domainmail0512mo
			//donc on garde que les chiffres pour avoir le quota et on a besoin de l'identifiant pour la création de la facture
			//on a donc refid:quota
			$id  = $record["rowid"] . ":" . preg_replace('/\D/', '', $record["ref"]);
			$txt = $record["label"];
			print "<option value=\"" . $id . "\">" . $txt . "</option>";
		}
		print "</select>\n";
	}
	print " pour le stockage <b>total</b> de votre domaine personnel et de toutes les boites mails</p>
	     </div>

	     <div>
      	     <p style=\"margin:30px 0 0 0;\"><b>&#9670; A quel tarif estimez-vous cette prestation ?</b></p>
	     <p style=\"margin:8px 0 8px 25px;\">&#x25BA; Tarif de l'hébergement: <input class=\"forms\" style=\"text-align:center\" type=\"text\" name=\"estimationTarif\" size=\"3\" maxlength=\"6\" value=\"1,8\" /> €uros par mois</p>

<p style=\"margin:8px 0 0 25px;\">&#x25BA; Périodicité des versements: <select class=\"flat maxwidth100\" name=\"periodicite\" id=\"period_id\"><option value=\"12\">Annuelle</option><option value=\"6\">Semestrielle</option></select></p>

<p style=\"margin:8px 0 0 25px;\">&#x25BA;  Mode de règlement envisagé: <select class=\"flat maxwidth100\" name=\"paiement\" id=\"paiement_id\"><option value=\"paypal\">Paypal / CB</option><option value=\"virement\">Virement bancaire</option><option value=\"cheque\">Chèque</option></select></p>
	     </div>

	     <div style=\"text-align:center\">
	     <input type=\"submit\" value=\"Demander la création de l'hébergement du domaine\">
	     </div>
	</div>
    </div>\n";
	print '</form>';

	//<option value=\"3\">Trimestrielle</option><option value=\"1\">Mensuelle</option>
}


llxHeader('', "Console de gestion utilisateur pour SudOuest", "");

$form = new Form($db);
$companystatic = new Societe($db);
$contactstatic = new Contact($db);
$adh = new Adherent($db);
$adh->fetch_login($user->login);

//print_r();

// Show navigation bar
if (empty($action)) {
	print "<p>Bienvenue dans votre console de <b>gestion administrative</b> de votre compte.</p>";
	print "<a href=\"index.php\">Retourner à l'accueil du module</a>";
}
if ($action == "ouvrirHebergement") {
	$existant = $adh->array_options["options_domaine"];
	if ($existant) {
		print "<p>Vous avez déjà un domaine personnel hébergé sur les serveurs de l'association : $existant</p>";
		print "<p>Vous pouvez consulter la documentation pour savoir comment l'utiliser ...</p>";
	}
	//Verification adresse mail dispo
	else if (GETPOST("votreDomaineMail")) {
		$adresseSouhaitee = trim(strtolower(GETPOST("votreDomaineMail")));
		$modo = new sudouestModoboa($db);
		if ($modo->testDomaine($adresseSouhaitee)) {
			print "<p style=\"background: #fee; border: 1px solid red; border-radius: 5px; padding: 5px;\"><b>Erreur, ce domaine ($adresseSouhaitee) est déjà utilisé !!!  ... veuillez nous contacter en cas de problème !</b></p>";
			print_form_ouvertureDomaineHebergeSOO();
		}
		//Tout est ok pour créer la facture et ouvrir le compte ...
		else {
			dol_syslog(" début de la création de la facture");
			//Creation de la facture
			$invoice = new Facture($db);
			$customer = $companystatic;

			if (!$error) {
				if (!($adh->fk_soc > 0)) {
					$langs->load("errors");
					$errmsg = $langs->trans("ErrorMemberNotLinkedToAThirpartyLinkOrCreateFirst");
					$error++;
					$sooDetailsErreur .= "Ce compte utilisateur n'est pas attaché à un Tiers<br />";
				}
			}
			dol_syslog("  création de la facture étape 1 ($error)");
			if (!$error) {
				$result = $customer->fetch($adh->fk_soc);
				if ($result <= 0) {
					$errmsg = $customer->error;
					$errmsgs = $acct->errors;
					$error++;
				}
			}
			dol_syslog("  création de la facture étape 2 ($error)");
			if (!$error) {
				dol_syslog("   brouillon de facture");
				// Create draft invoice
				$invoice->type = Facture::TYPE_STANDARD;
				$invoice->cond_reglement_id = $customer->cond_reglement_id;
				if (empty($invoice->cond_reglement_id)) {
					$paymenttermstatic = new PaymentTerm($db);
					$invoice->cond_reglement_id = $paymenttermstatic->getDefaultId();
					if (empty($invoice->cond_reglement_id)) {
						$error++;
						$errmsg = 'ErrorNoPaymentTermRECEPFound';
					}
				}
				dol_syslog("   pour adhérent dont la société rattachée est fk_soc:" . $adh->fk_soc);
				$invoice->socid = $adh->fk_soc;

				$datesubscription = date("Y-m-d");
				dol_syslog("   à la date du:" . $datesubscription);
				$invoice->date = $datesubscription;

				$result = $invoice->create($user);
				if ($result <= 0) {
					$errmsg = $invoice->error;
					$errmsgs = $invoice->errors;
					$error++;
				}
			}

			dol_syslog("  création de la facture étape 3  ($error)");
			if (!$error) {
				// Add line to draft invoice
				$idprodsubscription = explode(":", GETPOST("domainmailQuota"))[0];
				//ici on utilise les codes produits du formulaire expédié (erics)
				$vattouse = get_default_tva($mysoc, $mysoc, $idprodsubscription);
				//print xx".$vattouse." - ".$mysoc." - ".$customer;exit;
				$label = "Hébergement domaine personnel ($adresseSouhaitee)";
				$dateend = mktime(0, 0, 0, date("m") + GETPOST("periodicite"),   date("d"),   date("Y"));
				$datesubend = date("Y-m-d", $dateend);
				$total = GETPOST("estimationTarif") * GETPOST("periodicite");
				dol_syslog("   ajout d'une ligne sur la facture : $label ref($idprodsubscription) pour la période du $datesubscription au $datesubend au prix libre de $total ...");

				$result = $invoice->addline($label, GETPOST("estimationTarif"), GETPOST("periodicite"), $vattouse, 0, 0, $idprodsubscription, 0, $datesubscription, $datesubend, 0, 0, '', 'TTC', GETPOST("estimationTarif"), 1);
				if ($result <= 0) {
					$errmsg = $invoice->error;
					$error++;
				}
				//On ajoute une ligne "à zéro" qui indique le nom du domaine hébergé ... c'est mieux :)
				$result = $invoice->addline($label, 0, 0, $vattouse, 0, 0, '', 0, '', '', 0, 0, '', 'TTC', 0, 1);
				if ($result <= 0) {
					$errmsg = $invoice->error;
					$error++;
				}
			}

			dol_syslog("  création de la facture étape 4  ($error)");
			if (!$error) {
				// Validate invoice ... sauf que non, il faut la transformer en facture recurente pour ensuite avoir la facturation automatique
				// qui sera en place ...
				/*
	      dol_syslog("   validation de la facture ...");
	      $result=$invoice->validate($user);
	      if ($result <= 0)
	      {
	      $errmsg=$invoice->error;
	      $errmsgs=$invoice->errors;
	      $error++;
	      }
	    */

				//On envoie plutot un mail aux administrateurs pour leur expliquer ce qu'il faut faire ...
				global $conf, $langs;
				global $dolibarr_main_url_root;

				require_once DOL_DOCUMENT_ROOT . '/core/class/CMailFile.class.php';

				$msgishtml = 1;

				// Define $msg
				$mesg = '';

				$subject = "[" . $conf->global->MAIN_INFO_SOCIETE_NOM . "] Mise en place d'une facture automatique pour " . $adh->firstname . " " . $adh->lastname;
				$domaine = trim(strtolower(GETPOST("votreDomaineMail")));

				$factureuri = $dolibarr_main_url_root . "/compta/facture.php?facid=" . $invoice->id;

				$ipAddress = $_SERVER['REMOTE_ADDR'];
				if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
					$ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
				}

				$mesg .= "<p>Bonjour,<br />
une nouvelle commande est en cours pour un domaine hébergé et vous devez manuellement la valider pour mettre en place la facturation automatique.</p>

<p>Vous trouverez ci-dessous les informations relatives à cette commande:</p>
<ul>
  <li>Identifiant: $login</li>
  <li>Domaine: $domaine</li>
  <li>Périodicité de facturation (en mois): " . GETPOST("periodicite") . "</li>
  <li>Choix du tarif mensuel pour ce service: " . GETPOST("estimationTarif") . "</li>
  <li>Numéro de facture provisoire : " . $invoice->ref . "</li>
  <li>Adresse IP du client lors de la demande : $ipAddress</li>
</ul>

<p>Cette facture est à transformer en facturation automatique, veuillez donc suivre les étapes suivantes:</p>
<ul>
  <li>identifiez vous avec votre compte secrétaire ou trésorier sur <a href=\"$dolibarr_main_url_root\">$dolibarr_main_url_root</a></li>
  <li>cliquez sur l'onglet Compta/Tréso</li>
  <li>cliquez sur la facture provisoire " . $invoice->ref . " -> <a href=\"$factureuri\">$factureuri</a></li>
  <li>cliquez sur le bouton \"convertir en facture modèle\" pour en faire une source de facture automatique</li>
  <li>complétez ce forumaire :
  <ul>
     <li>titre de facture automatique comme par exemple \"Hébergement domaine personnel de " . $adh->firstname . " " . $adh->lastname . "\"</li>
     <li>configurez la récurrence : " . GETPOST("periodicite") . " mois</li>
     <li>date pour la prochaine facture : cliquer sur le lien \"maintenant\"</li>
     <li>nombre maximum de génération : 1000 (?)</li>
     <li>statut des factures générées : validée à payer</li>
     <li>puis cliquez sur le bouton créer</li>
  </ul>
  <li>c'est tout, la facture devrait se faire toute seule ... pensez quand même à vérifier demain si c'est bien le cas !</li>
  <li>normalement une facture sera générée automatiquement à échéance</li>
</ul>
\n\n";

				$mesg .= "<pre>\n--\nEnvoyé par le module sudouest sur dolibarr</pre>";

				dol_syslog("  send_mail pour demander aux responsables de faire la création de facture automatique ...");

				$mailfile = new CMailFile(
					$subject,
					$conf->notification->email_from,
					$conf->notification->email_from,
					$mesg,
					array(),
					array(),
					array(),
					'',
					'',
					0,
					$msgishtml
				);

				if (!$mailfile->sendfile()) {
					$langs->trans("errors");
					$this->error = "Erreur d'envoi mail : " . $mailfile->error;
					$retour = -1;
				}
			}


			if (!$error) {
				$db->commit();
				dol_syslog("   facture validée ...");
			} else {
				$db->rollback();
				dol_syslog("   validation de facture annulée ...");
			}

			// Si pas d'erreur de facturation on passe a la suite : creation du compte dans modoboa
			if (!$error) {
				dol_syslog("   création du compte dans modoboa ... (1)");
				//Et on créé son adresse mail sur le domaine, on gèrera plus tard la relance de paiement ... erics3
				$modo = new sudouestModoboa($db);
				$domaine = trim(strtolower(GETPOST("votreDomaineMail")));
				$tabdomainmailQuota = explode(":", GETPOST("domainmailQuota"));
				$domainmailQuota = $tabdomainmailQuota[1];
				dol_syslog("   création du domaine dans modoboa ... (2)");
				$code = $modo->createDomaine($domaine, $domainmailQuota);
				if ($code <= 0) {
					print "<p>Erreur de création du domaine ...</p>\n";
				}

				unset($modo);
				$modo = new sudouestModoboa($db);

				//Ensuite il faut créer l'administrateur du domaine ... c'est à dire l'adhérent en cours !
				$login = "admin@$domaine";
				$pass = getRandomPassword(false) . "Bu43-9";
				$code = $modo->createMail($login, "Administrateur " . $adh->firstname, $adh->lastname, $pass, $adh->email, $domaine, 0, "Resellers");
				if ($code <= 0) {
					//seconde tentative au cas ou
					$code = $modo->createMail($login, "Administrateur " . $adh->firstname, $adh->lastname, $pass, $adh->email, $domaine, 0, "Resellers");
					if ($code <= 0) {
						print "<p><b>Erreur de création du compte Administrateur du domaine ...</b></p>\n";
					}
				}

				unset($modo);
				$modo = new sudouestModoboa($db);
				//Et les comptes speciaux (postmaster and abuse)
				$alias = "postmaster@$domaine";
				$recipients = array($conf->global->SOO2_MAIL_POSTMASTER);
				$code = $modo->createAlias($alias, $recipients);
				if ($code <= 0) {
					print "<p>Erreur de création de l'alias $alias sur le domaine ...</p>\n";
				}

				unset($modo);
				$modo = new sudouestModoboa($db);
				//Et les comptes speciaux (postmaster and abuse)
				$alias = "abuse@$domaine";
				$recipients = array($conf->global->SOO2_MAIL_ABUSE);
				$code = $modo->createAlias($alias, $recipients);
				if ($code <= 0) {
					print "<p>Erreur de création de l'alias $alias sur le domaine ...</p>\n";
				}

				print "<p>La création de votre domaine sur modoboa est maintenant terminée. Il nous reste à faire la suite (gestion administrative etc.).</p>";
				print "<p>Pour administrer votre domaine vous devrez utiliser les informations affichées ci dessous (ne les perdez pas !) :</p>";
				print "<p><ul>
<li>Adresse de connexion: <a href=\"" . $conf->global->SOO2_MAIL_WEB_URL  . "\">" . $conf->global->SOO2_MAIL_WEB_URL . "</a></li>
<li>Identifiant mail: $login</li>
<li>Mot de passe mail: $pass</li>
</ul></p>\n";
				print "<p>Dès que la phase de tests du nouveau sud-ouest sera plus avancée vous recevrez par email les paramètres de connexion IMAP/SMTP etc.</p>\n";

				//Si tout a marché on "update" l'adhérent pour qu'on sache quelle adresse sur le domaine hébergé par l'asso il a ...
				dol_syslog("   mise à jour du compte pour associer le domaine modoboa ...");
				$adh->array_options["options_domaine"] = $adresseSouhaitee;
				$adh->update($user, 1);
			} else {
				print "<p>Erreur de création du domaine sur modoboa, veuillez <a href=\"/support/\">ouvrir un ticket</a> (il faut se créer un compte si vous n'en avez pas déjà un sur la plate-forme d'assistance) et fournir le plus possible d'informations pour qu'un administrateur puisse vous aider ...</p>";
				print "<p>Détails de l'erreur;</p>\n";
				print $sooDetailsErreur;
			}
		}
	} else {
		print_form_ouvertureDomaineHebergeSOO();
	}
}


llxFooter();

$db->close();
