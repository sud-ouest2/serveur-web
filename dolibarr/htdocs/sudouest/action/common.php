<?php
require_once DOL_DOCUMENT_ROOT.'/societe/class/societe.class.php';
require_once DOL_DOCUMENT_ROOT.'/contact/class/contact.class.php';
require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherent.class.php';
require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherent_type.class.php';
require_once DOL_DOCUMENT_ROOT.'/sudouest/class/modoboa.class.php';
require_once DOL_DOCUMENT_ROOT.'/product/class/product.class.php';
require_once DOL_DOCUMENT_ROOT.'/compta/facture/class/facture.class.php';
require_once DOL_DOCUMENT_ROOT.'/compta/facture/class/paymentterm.class.php';
require_once DOL_DOCUMENT_ROOT .'/core/lib/security2.lib.php';

function estimationTarifs() {
  return "	     <div class=\"col-md-4 order-last\" style=\"background: #fee; border: 2px solid red; border-radius: 5px; padding: 5px;\">
	      	<p> Estimation de la valeur du service pour pérenniser le système :
		   <ul>
			<li>1€ par mois pour 512Mo</li>
			<li>1,8€ par mois pour 1Go</li>
			<li>2,5€ par mois pour 2Go</li>
			<li>2,8€ par mois pour 3Go</li>
		   </ul>
		</p>
                <span style=\"color: #FF9900;\">&#x25B2;</span> Valeurs indicatives suite aux demandes reçues, mise à jour août 2017. Si vous estimez devoir payer moins, faites. Si vous souhaitez verser plus, faites aussi
	     </div>
";
}

function estimationTarifsSympa() {
  return "	     <div class=\"col-md-4 order-last\" style=\"background: #fee; border: 2px solid red; border-radius: 5px; padding: 5px;\">
	      	<p> Estimation de la valeur du service pour pérenniser le système :
		   <ul>
			<li>3€ par mois pour une centaine d'abonnés à vos listes</li>
			<li>8€ par mois pour environ 400 abonnés à vos listes</li>
			<li>12€ par mois pour environ 800 abonnés à vos listes</li>
			<li>20€ par mois pour environ 2000 abonnés à vos listes</li>
		   </ul>
		</p>
                <span style=\"color: #FF9900;\">&#x25B2;</span> Valeurs indicatives suite aux demandes reçues, mise à jour août 2017. Si vous estimez devoir payer moins, faites. Si vous souhaitez verser plus, faites aussi
	     </div>
";
}

function print_form_ouvertureMailSOO($formulaire_complet=1) {
  global $db,$conf,$langs;

  print "	<div class=\"row\">\n";

  print estimationTarifs();

  print "	     <div class=\"col-md-8 order-first\">\n";
  if($formulaire_complet == 1) {
    print '<form action="'.$_SERVER["PHP_SELF"].'" method="POST" name="ouvrirCompteMail">'."\n";
    print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'" / >';
    print '<input type="hidden" name="entity" value="'.$entity.'" />';
    print '<input type="hidden" name="action" value="ouvrirCompteMail" />';
  }

	print "    <div style=\"text-align: left; background: #eee; padding: 5px; margin: 5px;\">
	     &nbsp;&nbsp;<b>- Obtenir une adresse mail sur un de nos domaines (" . $conf->global->SOO2_MAIL_DOMAINS . ")</b>\n";
	print "
  		     	  <div style=\"border-left: 3p	x dotted #000000; margin-left: 10px; margin-bottom: 15px;\">
		<p class=\"infos\" style=\"margin: 8px 0 0 15px;\">
			Exemple: Vous désirez une adresse mail du type marcel.cromagnon@domaine.ext ou ernestine@domaine.ext
		</p>
	      	<p style=\"margin: 0 0 0 30px;\">
			&#x2514; L'adresse mail que vous souhaiteriez: <input class=\"forms\" type=\"text\" name=\"adresseSouhaitee\" size=\"15\" maxlength=\"25\" value=\"\" /> @
			  <select class=\"forms\" name=\"adresseSouhaiteeDomaine\">\n";
	$domaines_internes = explode(',', $conf->global->SOO2_MAIL_DOMAINS);
	foreach ($domaines_internes as &$ledomaine) {
		print "			    <option value=\"$ledomaine\">$ledomaine</option>\n";
	}

	print "			  </select>
		</p>
	                   </div>

	      	<p style=\"margin:30px 0 0 0;\"><b>&#9670; De quel espace de stockage avez vous besoin ?</b> \n";
  //On cherche tous les services dont le nom commence par mailbox et on fait une liste ...
  //erics2
  $paymenttermstatic=new PaymentTerm($db);


  $sql = "SELECT * FROM ".MAIN_DB_PREFIX."product WHERE ref LIKE 'mailbox%' ORDER BY ref ASC";
  $res = $db->query($sql);
  if ($res)
    {
      print "<select class=\"flat maxwidth200\" name=\"mailboxQuota\" id=\"space_id\">\n";
      $record = array();
      while ($record = $db->fetch_array($res))
	{
	  //astuce, on a besoin du quota pour creer la boite, la nomenclature des services est du genre mailbox0512mo
	  //donc on garde que les chiffres pour avoir le quota et on a besoin de l'identifiant pour la création de la facture
	  //on a donc refid:quota
	  $id  = $record["rowid"] . ":" . preg_replace('/\D/', '', $record["ref"]);
	  $txt = $record["label"];
	  print "<option value=\"" . $id . "\">" . $txt . "</option>";
	}
      print "</select>\n";
    }
  print " pour le stockage total de votre boite mail</p>

      	     <p style=\"margin:30px 0 0 0;\"><b>&#9670; A quel tarif estimez-vous cette prestation ?</b></p>
	     <p style=\"margin:8px 0 8px 25px;\">&#x25BA; Tarif de l'hébergement: <input class=\"forms\" style=\"text-align:center\" type=\"text\" name=\"estimationTarif\" size=\"3\" maxlength=\"6\" value=\"1\" /> €uros par mois</p>

<p style=\"margin:8px 0 0 25px;\">&#x25BA; Périodicité des versements: <select class=\"flat maxwidth100\" name=\"periodicite\" id=\"period_id\"><option value=\"12\">Annuelle</option><option value=\"6\">Semestrielle</option></select></p>\n";
  if($formulaire_complet == 1) {
    print "<p style=\"margin:8px 0 0 25px;\">&#x25BA;  Mode de règlement envisagé: <select class=\"flat maxwidth100\" name=\"paiement\" id=\"paiement_id\"><option value=\"paypal\">Paypal / CB</option><option value=\"virement\">Virement bancaire</option><option value=\"cheque\">Chèque</option></select></p>\n";
  }

  if($formulaire_complet == 1) {
    print "
	     <div style=\"text-align:center\">
	     <input type=\"submit\" value=\"Demander la création du compte mail\">
	     </div>\n";
  }

  print "	</div>\n";
  if($formulaire_complet == 1) {
    print '</form>';
  }
  print "  	</div>\n";

  print "    </div>\n";

  //<option value=\"3\">Trimestrielle</option><option value=\"1\">Mensuelle</option>
}
