<?php
/*
 * Copyright (C) 2017      Eric Seigne        <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 *  \file       htdocs/sudouest/action/index.php
 *  \ingroup    sudouest
 *  \brief      Home page of sudouest
 */

require '../../main.inc.php';
require 'common.php';

$action=GETPOST('action','alpha');

if (!$user->rights->sudouest->actions->lire)
  accessforbidden();

/*
 * View
 */



llxHeader('',"Console de gestion utilisateur pour SudOuest","");

$form=new Form($db);
$companystatic=new Societe($db);
$contactstatic=new Contact($db);
$adh=new Adherent($db);
$adh->fetch_login($user->login);

//print_r();

// Show navigation bar
if (empty($action))
{
  print "<p>Bienvenue dans votre console de <b>gestion administrative</b> de votre compte.</p>";
  print "<a href=\"index.php\">Retourner à l'accueil du module</a>";
}
if ($action == "ouvrirCompteMail")
{
  $mailexistant = $adh->array_options["options_mailsoo"];
  if($mailexistant) {
    print "<p>Vous avez déjà une adresse email hébergée sur un domaine de l'association : $mailexistant</p>";
    print "<p>Vous pouvez consulter la documentation pour savoir comment l'utiliser ...</p>";
    print "<p>Si vous voulez créer un autre compte mail \"enfant\" de votre compte principal vous pouvez suivre cette <a href=\"\">nouvelle procédure</a> (en cours de dev)...</p>";
  }
  //Verification adresse mail dispo
  else if(trim(GETPOST("adresseSouhaitee"))) {
    $adresseSouhaitee = trim(strtolower(GETPOST("adresseSouhaitee"))) . "@" . trim(GETPOST("adresseSouhaiteeDomaine"));
      $modo = new sudouestModoboa($db);
      if($modo->testMail($adresseSouhaitee)) {
	$adresseSouhaitee2 = trim(strtolower(GETPOST("adresseSouhaitee"))) . rand(10,9999);
	print "<p style=\"background: #fee; border: 1px solid red; border-radius: 5px; padding: 5px;\"><b>Erreur, cette adresse ($adresseSouhaitee) est déjà utilisée ... veuillez en choisir une autre (exemple $adresseSouhaitee2) et validez à nouveau ce formulaire</b></p>";
	print_form_ouvertureMailSOO();
      }
      //Tout est ok pour créer la facture et ouvrir le compte ...
      else {
	dol_syslog(" début de la création de la facture");
	//Creation de la facture
	$invoice=new Facture($db);
	$customer=$companystatic;

	if (! $error)
	  {
	    if (! ($adh->fk_soc > 0))
	      {
		$langs->load("errors");
		$errmsg=$langs->trans("ErrorMemberNotLinkedToAThirpartyLinkOrCreateFirst");
		$error++;
	      }
	  }
	if (! $error)
	  {
	    $result=$customer->fetch($adh->fk_soc);
	    if ($result <= 0)
	      {
		$errmsg=$customer->error;
		$errmsgs=$acct->errors;
		$error++;
	      }
	  }

	if (! $error)
	  {
	    dol_syslog("   brouillon de facture");
	    // Create draft invoice
	    $invoice->type= Facture::TYPE_STANDARD;
	    $invoice->cond_reglement_id=$customer->cond_reglement_id;
	    if (empty($invoice->cond_reglement_id))
	      {
		$paymenttermstatic=new PaymentTerm($db);
		$invoice->cond_reglement_id=$paymenttermstatic->getDefaultId();
		if (empty($invoice->cond_reglement_id))
		  {
		    $error++;
		    $errmsg='ErrorNoPaymentTermRECEPFound';
		  }
	      }
	    dol_syslog("   pour adhérent dont la société rattachée est fk_soc:" .$adh->fk_soc);
	    $invoice->socid=$adh->fk_soc;

	    $datesubscription=date("Y-m-d");
	    dol_syslog("   à la date du:" .$datesubscription);
	    $invoice->date=$datesubscription;
	    
	    $result=$invoice->create($user);
	    if ($result <= 0)
	      {
		$errmsg=$invoice->error;
		$errmsgs=$invoice->errors;
		$error++;
	      }
	  }

	if (! $error)
	  {
	    // Add line to draft invoice
	    $idprodsubscription=explode(":",GETPOST("mailboxQuota"))[0];
	    //ici on utilise les codes produits du formulaire expédié (erics)
	    $vattouse=get_default_tva($mysoc, $mysoc, $idprodsubscription);
	    //print xx".$vattouse." - ".$mysoc." - ".$customer;exit;
	    $login = trim(strtolower(GETPOST("adresseSouhaitee"))) . "@" . trim(GETPOST("adresseSouhaiteeDomaine"));
	    $label = "Hébergement de la boite courriel $login";
	    $dateend = mktime(0, 0, 0, date("m") + GETPOST("periodicite"),   date("d"),   date("Y"));
	    $datesubend=date("Y-m-d", $dateend);
	    $total=GETPOST("estimationTarif")*GETPOST("periodicite");
	    dol_syslog("   ajout d'une ligne sur la facture : $label ref($idprodsubscription) pour la période du $datesubscription au $datesubend au prix libre de $total ...");

	    $result=$invoice->addline($label,GETPOST("estimationTarif"),GETPOST("periodicite"),$vattouse,0,0,$idprodsubscription,0,$datesubscription,$datesubend,0,0,'','TTC',GETPOST("estimationTarif"),1);
	    if ($result <= 0)
	      {
		$errmsg=$invoice->error;
		$error++;
	      }

	    //On ajoute une ligne "à zéro" qui indique le nom de l'adresse mail hébergée ... c'est mieux :)
	    $result=$invoice->addline($label,0,0,$vattouse,0,0,'',0,'','',0,0,'','TTC',0,1);
	    if ($result <= 0)
	      {
		$errmsg=$invoice->error;
		$error++;
	      }

	  }
	
	if (! $error)
	  {
	    // Validate invoice ... sauf que non, il faut la transformer en facture recurente pour ensuite avoir la facturation automatique
	    // qui sera en place ...
	    /*
	      dol_syslog("   validation de la facture ...");
	      $result=$invoice->validate($user);
	      if ($result <= 0)
	      {
	      $errmsg=$invoice->error;
	      $errmsgs=$invoice->errors;
	      $error++;
	      }
	    */

	    //On envoie plutot un mail aux administrateurs pour leur expliquer ce qu'il faut faire ...
	    global $conf,$langs;
	    global $dolibarr_main_url_root;

	    require_once DOL_DOCUMENT_ROOT.'/core/class/CMailFile.class.php';

	    $msgishtml=1;

	    // Define $msg
	    $mesg = '';

	    $subject = "[" . $conf->global->MAIN_INFO_SOCIETE_NOM . "] Mise en place d'une facture automatique pour " . $adh->firstname . " " . $adh->lastname;

	    $factureuri = $dolibarr_main_url_root . "/compta/facture.php?facid=" . $invoice->id;

	    
	    $ipAddress = $_SERVER['REMOTE_ADDR'];
	    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	      $ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
	    }

	    $mesg.= "<p>Bonjour,<br />
une nouvelle commande est en cours et vous devez manuellement la valider pour mettre en place la facturation automatique.</p>

<p>Vous trouverez ci-dessous les informations relatives à cette commande:</p>
<ul>
  <li>Identifiant: $login</li>
  <li>Périodicité de facturation (en mois): " . GETPOST("periodicite") . "</li>
  <li>Choix du tarif mensuel pour ce service: " . GETPOST("estimationTarif") . "</li>
  <li>Numéro de facture provisoire : " . $invoice->ref . "</li>
  <li>Adresse IP du client lors de la demande : $ipAddress</li>
</ul>

<p>Cette facture est à transformer en facturation automatique, veuillez donc suivre les étapes suivantes:</p>

<ul>
  <li>identifiez vous avec votre compte secrétaire ou trésorier sur <a href=\"$dolibarr_main_url_root\">$dolibarr_main_url_root</a></li>
  <li>cliquez sur l'onglet Compta/Tréso</li>
  <li>cliquez sur la facture provisoire " . $invoice->ref . " -> <a href=\"$factureuri\">$factureuri</a></li>
  <li>cliquez sur le bouton \"convertir en facture modèle\" pour en faire une source de facture automatique</li>
  <li>complétez ce formulaire :
  <ul>
     <li>titre de facture automatique comme par exemple \"Hébergement courriel " . $adh->firstname . " " . $adh->lastname . "\"</li>
     <li>configurez la récurrence : " . GETPOST("periodicite") . " mois</li>
     <li>date pour la prochaine facture : cliquer sur le lien \"maintenant\"</li>
     <li>nombre maximum de génération : 1000 (?)</li>
     <li>statut des factures générées : validée à payer</li>
     <li>puis cliquez sur le bouton créer</li>
  </ul>
  </li>
  <li>c'est tout, la facture devrait se faire toute seule ... pensez quand même à vérifier demain si c'est bien le cas !</li>
  <li>normalement une facture sera générée automatiquement à échéance</li>
</ul>

\n\n";
	    
	    $mesg.= "<pre>\n--\nEnvoyé par le module sudouest sur dolibarr</pre>";
	    
	    dol_syslog("  send_mail pour demander aux responsables de faire la création de facture automatique ...");
	    
	    $mailfile = new CMailFile($subject,
				      $conf->notification->email_from,
				      $conf->notification->email_from,
				      $mesg,
				      array(),
				      array(),
				      array(),
				      '',
				      '',
				      0,
				      $msgishtml
				      );
	    
	    if (! $mailfile->sendfile())
	      {
		$langs->trans("errors");
		$this->error="Erreur d'envoi mail : " . $mailfile->error;
		$retour = -1;
	      }
	    
	  }
	
	/*
	// Add payment onto invoice
	if ($option == 'bankviainvoice' && $accountid)
	{
	require_once DOL_DOCUMENT_ROOT.'/compta/paiement/class/paiement.class.php';
	require_once DOL_DOCUMENT_ROOT.'/compta/bank/class/account.class.php';
	require_once DOL_DOCUMENT_ROOT.'/core/lib/functions.lib.php';
	
	$amounts[$invoice->id] = price2num($subscription);
	$paiement = new Paiement($db);
	$paiement->datepaye     = $paymentdate;
	$paiement->amounts      = $amounts;
	$paiement->paiementid   = dol_getIdFromCode($db,$operation,'c_paiement');
	$paiement->num_paiement = $num_chq;
	$paiement->note         = $label;
	
	    if (! $error)
	      {
		// Create payment line for invoice
		$paiement_id = $paiement->create($user);
		if (! $paiement_id > 0)
		  {
		    $errmsg=$paiement->error;
		    $errmsgs=$paiement->errors;
		    $error++;
		  }
	      }
	    
	    if (! $error)
                    {
                    	// Add transaction into bank account
                        $bank_line_id=$paiement->addPaymentToBank($user,'payment','(SubscriptionPayment)',$accountid,$emetteur_nom,$emetteur_banque);
                        if (! ($bank_line_id > 0))
                        {
                            $errmsg=$paiement->error;
                            $errmsgs=$paiement->errors;
	                        setEventMessages($paiement->error, $paiement->errors, 'errors');
                            $error++;
                        }
                    }

                    if (! $error)
                    {
                        // Update fk_bank into subscription table
                        $sql = 'UPDATE '.MAIN_DB_PREFIX.'subscription SET fk_bank='.$bank_line_id;
                        $sql.= ' WHERE rowid='.$crowid;

	                    $result = $db->query($sql);
                        if (! $result)
                        {
                            $error++;
                        }
                    }

                    if (! $error)
                    {
                        // Set invoice as paid
                    	$invoice->set_paid($user);
                    }

                    if (! $error)
                    {
						// Define output language
						$outputlangs = $langs;
						$newlang = '';
						if ($conf->global->MAIN_MULTILANGS && empty($newlang) && ! empty($_REQUEST['lang_id']))
							$newlang = $_REQUEST['lang_id'];
						if ($conf->global->MAIN_MULTILANGS && empty($newlang))
							$newlang = $customer->default_lang;
						if (! empty($newlang)) {
							$outputlangs = new Translate("", $conf);
							$outputlangs->setDefaultLang($newlang);
						}
                    	// Generate PDF (whatever is option MAIN_DISABLE_PDF_AUTOUPDATE) so we can include it into email
						//if (empty($conf->global->MAIN_DISABLE_PDF_AUTOUPDATE))

	                    $invoice->generateDocument($invoice->modelpdf, $outputlangs, $hidedetails, $hidedesc, $hideref);
                    }
                }
            }
	*/

        if (! $error)
        {
            $db->commit();
	    dol_syslog("   facture validée ...");
        }
        else
        {
            $db->rollback();
	    dol_syslog("   validation de facture annulée ...");
        }
	
        // Si pas d'erreur de facturation on passe a la suite : creation du compte dans modoboa
        if (! $error)
        {
	    dol_syslog("   création du compte dans modoboa ... (1)");
	    //Et on créé son adresse mail sur le domaine, on gèrera plus tard la relance de paiement ... erics3
	    $modo = new sudouestModoboa($db);
	    $login = trim(strtolower(GETPOST("adresseSouhaitee"))) . "@" . GETPOST("adresseSouhaiteeDomaine");
	    $tabmailboxQuota = explode(":",GETPOST("mailboxQuota"));
	    $mailboxQuota = $tabmailboxQuota[1];
	    $pass = getRandomPassword(false) . "A2:8Z";
	    dol_syslog("   création du compte dans modoboa ... (2)");
	    if($modo->createMail($login, $adh->firstname, $adh->lastname, $pass, $adh->email, GETPOST("adresseSouhaiteeDomaine"), $mailboxQuota, "SimpleUsers") == 1) {
	    
	      print "<p>La création de votre compte sur modoboa (outil de gestion de votre boite mail) est maintenant terminée. Il nous reste à faire la suite (gestion administrative etc.) ... La mauvaise nouvelle c'est que votre mot de passe n'a pas été propagé car dolibarr (outil de suivi administratif) le stocke de manière chiffrée. De ce fait pour vous connecter sur votre infrastructure mail vous devrez utiliser les informations affichées ci dessous (ne les perdez pas !) :</p>";
	      print "<p><ul>
<li>Adresse de connexion: <a href=\"https://mail.sud-ouest2.org/\">https://mail.sud-ouest2.org/</a></li>
<li>Identifiant mail: $login</li>
<li>Mot de passe mail: $pass</li>
</ul></p>\n";
	      //print "<p>Dès que la phase de tests du nouveau sud-ouest sera plus avancée vous recevrez par email les paramètres de connexion IMAP/SMTP etc.</p>\n";
	      //Si tout a marché on "update" l'adhérent pour qu'on sache quelle adresse sur le domaine hébergé par l'asso il a ...
	      dol_syslog("   mise à jour du compte pour associer l'adresse modoboa ...");
	      $adh->array_options["options_mailsoo"] = $adresseSouhaitee;
	      $adh->update($user, 1);

	      /*
	    //On envoie plutot un mail aux administrateurs pour leur expliquer ce qu'il faut faire ...
	    global $conf,$langs;
	    global $dolibarr_main_url_root;

	    require_once DOL_DOCUMENT_ROOT.'/core/class/CMailFile.class.php';

	    $msgishtml=1;

	    // Define $msg
	    $mesg = '';

	    $subject = "[" . $conf->global->MAIN_INFO_SOCIETE_NOM . "] Mise en place d'une facture automatique pour " . $adh->firstname . " " . $adh->lastname;

	    $factureuri = $dolibarr_main_url_root . "/compta/facture.php?facid=" . $invoice->id;
	    $mesg.= "<p>Bonjour,<br />
une nouvelle commande est en cours et vous devez manuellement la valider pour mettre en place la facturation automatique.</p>
	      */
	      
	    }
	    else {
	      print "<p>Erreur de création de votre compte sur modoboa ! Les administrateurs ont été avertis mais peut-être que vous pouvez re-tenter le coup en vérifiant bien les différents champs saisis ... si ça ne marche pas deux fois de suite patientez qu'un admin prenne la suite.</p>";
	    }
	    
	}
	else {
	  print "<p>Erreur de création du compte sur modoboa, veuillez <a href=\"/support/\">ouvrir un ticket</a> (il faut se créer un compte si vous n'en avez pas déjà un sur la plate-forme d'assistance) et fournir le plus possible d'informations pour qu'un administrateur puisse vous aider ...</p>";
	}
      }
  }
  else {
    print_form_ouvertureMailSOO();
  }  
}

llxFooter();

$db->close();

