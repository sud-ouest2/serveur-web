<?php
/*
 * Copyright (C) 2017      Eric Seigne        <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 *  \file       htdocs/sudouest/action/index.php
 *  \ingroup    sudouest
 *  \brief      Home page of sudouest
 */

require '../../main.inc.php';
require 'common.php';

$action=GETPOST('action','alpha');

if (!$user->rights->sudouest->actions->lire)
  accessforbidden();

/*
 * View
 */

llxHeader('',"Console de gestion utilisateur pour SudOuest","");

$form=new Form($db);
$companystatic=new Societe($db);
$contactstatic=new Contact($db);
$adh=new Adherent($db);
$adh->fetch_login($user->login);

//print_r();

// Show navigation bar
if (empty($action))
{
  print "<p>Bienvenue dans votre console de <b>gestion administrative</b> de votre compte.</p>";

  print "<h3>Vos services actifs</h3>";

  print "<ul>\n";
  $nbsa = 0;
  $mailexistant = $adh->array_options["options_mailsoo"];
  if($mailexistant) {
    $nbsa++;
    print "  <li>&#9745 Vous avez une adresse mail sur un domaine hébergé par l'association : <b>$mailexistant</b>.</li>\n";
    $aliasmailexistant = $adh->array_options["options_mailsooalias"];
    if($aliasmailexistant) {
      print "<ul><li>&#9744 Vous avez les alias suivants d'actifs: $aliasmailexistant</li></ul>\n";
    }
    print "<ul><li>&#9744 Vous pouvez demander la <a href=\"aliasmail.php?action=ouvrirAliasMail\">[création d'alias mails]</a> </li></ul>\n";
    print "<ul><li>&#9744 Vous pouvez 
<ul>
  <li>changer votre mot de passe mail via <a href=\"https://mail.sud-ouest2.org\">[modoboa]</a></li>
  <li>consulter et gérer vos mails en quarantaine via <a href=\"https://mail.sud-ouest2.org\">[modoboa]</a></li>
  <li>consulter vos mails en ligne via <a href=\"https://mail.sud-ouest2.org\">[modoboa]</a> ou <a href=\"https://mail.sud-ouest2.org/roundcube\">[roundcube]</a></li>
  <li>vous référer à la doc ci dessous pour configurer votre client mail habituel</li>
</ul>
</ul>\n";
  }
  $domaineexistant = $adh->array_options["options_domaine"];
  if($domaineexistant) {
    $nbsa++;
    print "  <li>&#9745 Vous avez un domaine personnel hébergé sur l'infrastructure de l'association : <b>$domaineexistant</b></li>\n";
    print "  <ul><li>&#9744 Vous pouvez gérer votre domaine, comptes mails et alias via <a href=\"https://mail.sud-ouest2.org\">[la console de gestion]</a>, pensez bien à vous identifier avec le compte admin@$domaineexistant ... </li></ul>\n";
  }
  
  $listesexistantes = $adh->array_options["options_sympa"];
  if($listesexistantes) {
    $domainelistes = str_replace("listes.","",$listesexistantes);
    $nbsa++;
    print "  <li>&#9745 Vous avez un hébergement de listes de diffusions : <b>$listesexistantes</b></li>\n";
    print "  <ul><li>&#9744 Vous pouvez gérer vos listes de diffusions via <a href=\"https://$listesexistantes/wws/\">[la console de gestion]</a> (et votre adresse listmaster@$domainelistes)</li></ul>\n";
  }
  print "</ul>";

  if($nbsa == 0)  {
    print "<p> Vous n'avez pas encore de service actif associé à votre compte ... </p>\n";
  }

  print "<h3>Ajouter un service</h3>";

  print "<ul>\n";
  if(!$mailexistant) {
    print "  <li>&#9744 Vous voulez une adresse mail sur un domaine hébergé par l'association ? <a href=\"./comptemail.php?action=ouvrirCompteMail\">[ouvrir un compte mail]</a></li>\n";
  }
  else {
    print "  <li>&#9744 Vous voulez créer un autre compte mail sur un domaine hébergé par l'association ? <a href=\"./comptemail.php?action=ouvrirCompteMail\">[ouvrir un compte mail]</a></li>\n";
  }
  if(! $domaineexistant) {
    print "  <li>&#9744 Vous voulez que l'association héberge les mails de votre propre domaine ? <a href=\"./hebergerdomaine.php?action=ouvrirHebergement\">[héberger mon domaine]</a></li>\n";
  }
  if(! $listesexistantes) {
    print "  <li>&#9744 Vous voulez ouvrir des listes de diffusions ? <a href=\"hebergerlistes.php?action=ouvrirListes\">[ouvrir un compte de liste de diffusion]</a></li>\n";
  }
  print "  <li>&#9744 Vous voulez faire un don ponctuel pour nous faire plaisir ? <a href=\"/comment-regler-une-adhesion-faire-un-don-payer-un-service/\">[faire un don]</a></li>\n";
  print "</ul>\n";

  print "<h3>Trouver de la documentation, de l'aide ou des copains</h3>";
  print "<ul>
  <li>Pour regarder vos mails et gérer votre boite : <a href=\"https://mail.sud-ouest2.org\">[interface web de gestion de votre compte]</a> ou <a href=\"https://mail.sud-ouest2.org/roundcube/\">[l'interface webmail roundcube plus avancée pour lire vos mails en ligne]</a></li>
  <li>Accédez à la [<a href=\"/configurer-votre-client-mail-pour-consulter-vos-courriels/\">documentation pour utiliser votre client mail habituel</a>]</li>
  <li>Documentation pour les adresses mails d'un domaine hébergé par l'association ? <a href=\"/category/documentation/\">[documentation]</a></li>
  <li>Abonnez vous à la liste de diffusion des membres de l'association pour bavarder entre vous <a href=\"https://listes.sud-ouest.org/wws/info/asso\">[liste asso]</a></li>
  <li>Abonnez vous à la liste de diffusion technique de l'association pour parler ... technique <a href=\"https://listes.sud-ouest.org/wws/info/tech\">[liste tech]</a></li>
  <li>En cas de problème vous pouvez ouvrir un ticket ... un admin bénévole essaiera d'y répondre le plus rapidement possible ... <a href=\"/support/\">[tickets]</a></li>
</ul>";

  print "<h3>Suivi administratif</h3>";
  print "<ul>
  <li>Vous pouvez consulter vos factures : <a href=\"../../compta/facture/list.php?leftmenu=customers_bills\">[factures]</a></li>
  <li>En cas de problème contactez-nous, en particulier si vous avez déjà payé votre hébergement ou cotisation cette année nous devons manuellement faire la mise à jour sur le nouveau système ... précisez donc le numéro de facture, la date et la référence de votre paiement et le trésorier, le secrétaire ou un bénévole sympa fera le nécessaire : <a href=\"/support\">[ouvrir un ticket]</a></li>
  <li>Si vous cherchez nos coordonnées bancaires et autres informations pour procéder à un paiement cliquez ici: <a href=\"/comment-regler-une-adhesion-faire-un-don-payer-un-service/\">[moyens de paiements]</a></li>
</ul>";

  print "<h3>Gestion de votre compte</h3>";
  print "<ul>
  <li>Vous pouvez modifier votre adresse mail de facturation ici : <a href=\"updatemail.php?action=updateMail\">[modifier mon mail de facturation]</a></li>
</ul>";

 }
if ($action == "ouvrirCompteMail")
{
  print "<p>Ooooops, cette page a changé d'adresse, <a href=\"./comptemail.php?action=ouvrirCompteMail\">veuillez aller ici</a> !</p>\n";
}

llxFooter();

$db->close();

