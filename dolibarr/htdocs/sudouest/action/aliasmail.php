<?php
/*
 * Copyright (C) 2017      Eric Seigne        <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 *  \file       htdocs/sudouest/action/index.php
 *  \ingroup    sudouest
 *  \brief      Home page of sudouest
 */

require '../../main.inc.php';
require 'common.php';

$action = GETPOST('action', 'alpha');

if (!$user->rights->sudouest->actions->lire)
	accessforbidden();

$adh = new Adherent($db);
$adh->fetch_login($user->login);

/*
 * View
 */

function print_form_ouvertureAliasSOO()
{
	global $db, $conf, $langs, $adh;

	$max_alias = 10;

	$mailexistant = $adh->array_options["options_mailsoo"];

	$aliasmailexistant = $adh->array_options["options_mailsooalias"];

	print '<form action="' . $_SERVER["PHP_SELF"] . '" method="POST" name="ouvrirAliasMail">' . "\n";
	print '<input type="hidden" name="token" value="' . $_SESSION['newtoken'] . '" / >';
	print '<input type="hidden" name="entity" value="' . $entity . '" />';
	print '<input type="hidden" name="action" value="ouvrirAliasMail" />';

	$tab = explode("@", $mailexistant);
	$domaine = $tab[1];


	print "    <div style=\"text-align: left; background: #eee; padding: 5px; margin: 5px;\">
	<div>\n";

	if ($aliasmailexistant) {
		$tabc = explode(',', $aliasmailexistant);
		if (count($tabc) > 4) {
			print "                <p style=\"background: #fee; border: 1px solid black; border-radius: 5px; padding: 5px; max-width: 80%;\">Pour rappel vous possedez déjà les alias suivants : $aliasmailexistant (limite maximale : $max_alias alias par compte)</p>\n";
		} else {
			$reste = $max_alias - count($tabc);
			print "                <p style=\"background: #efe; border: 1px solid black; border-radius: 5px; padding: 5px; max-width: 80%;\">Pour rappel vous possedez déjà les alias suivants : $aliasmailexistant (limite maximale : $max_alias alias par compte, il vous en reste donc encore $reste de libre)</p>\n";
		}
	}

	print "	     <br/>&nbsp;&nbsp;<b>- Obtenir un alias mail sur un de nos domaines (" . $conf->global->SOO2_MAIL_DOMAINS . ")</b>\n";
	print "	     <div style=\"border-left: 3px dotted #000000; margin-left: 10px; margin-bottom: 15px;\">
		<p class=\"infos\" style=\"margin: 8px 0 0 15px;\">
			Exemple: Vous désirez un alias toto@domaine.ext pour votre adresse mail <b>$mailexistant</b> ...
		</p>
	      	<p style=\"margin: 0 0 0 30px;\">
			&#x2514; L'alias mail que vous souhaiteriez: <input class=\"forms\" type=\"text\" name=\"adresseSouhaitee\" size=\"15\" maxlength=\"25\" value=\"\" /> @
			  <select class=\"forms\" name=\"adresseSouhaiteeDomaine\">\n";
	$domaines_internes = explode(',', $conf->global->SOO2_MAIL_DOMAINS);
	foreach ($domaines_internes as &$ledomaine) {
		print "			    <option value=\"$ledomaine\">$ledomaine</option>\n";
	}

	print "			  </select>
		</p>\n";


	print "
	     <div style=\"text-align:center\">
	     <input type=\"submit\" value=\"Demander la création de l'alias mail\">
	     </div>
	</div>
    </div>\n";
	print '</form>';
}


llxHeader('', "Console de gestion utilisateur pour SudOuest", "");

$form = new Form($db);
$companystatic = new Societe($db);
$contactstatic = new Contact($db);
$adh = new Adherent($db);
$adh->fetch_login($user->login);

//print_r();

// Show navigation bar
if (empty($action)) {
	print "<p>Bienvenue dans votre console de <b>gestion administrative</b> de votre compte.</p>";
	print "<a href=\"index.php\">Retourner à l'accueil du module</a>";
}
if ($action == "ouvrirAliasMail") {
	$mailexistant = $adh->array_options["options_mailsoo"];
	if (!$mailexistant) {
		print "<p>Vous n'avez pas encore d'adresse mail hébergée sur un domaine de l'association, veuillez commencer par un en créer une depuis la <a href=\"index.php\">[page d'accueil]</a>!</p>";
	}
	//Verification adresse mail dispo
	else if (GETPOST("adresseSouhaitee")) {

		$adresseSouhaitee = trim(strtolower(GETPOST("adresseSouhaitee"))) . "@" . GETPOST("adresseSouhaiteeDomaine");
		$aliasmailexistant = $adh->array_options["options_mailsooalias"];
		$modo = new sudouestModoboa($db);
		if ($modo->testMail($adresseSouhaitee)) {
			$adresseSouhaitee2 = trim(strtolower(GETPOST("adresseSouhaitee"))) . rand(10, 9999);
			print "<p style=\"background: #fee; border: 1px solid red; border-radius: 5px; padding: 5px;\"><b>Erreur, cette adresse ($adresseSouhaitee) est déjà utilisée ... veuillez en choisir une autre (exemple $adresseSouhaitee2) et validez à nouveau ce formulaire</b></p>";
			print_form_ouvertureAliasSOO();
		}
		//Tout est ok pour créer l'alias
		else {
			dol_syslog(" début de la création de l'alias");
			//On envoie plutot un mail aux administrateurs pour leur expliquer ce qu'il faut faire ...
			global $conf, $langs;
			global $dolibarr_main_url_root;

			require_once DOL_DOCUMENT_ROOT . '/core/class/CMailFile.class.php';

			$msgishtml = 1;

			// Define $msg
			$mesg = '';

			$subject = "[" . $conf->global->MAIN_INFO_SOCIETE_NOM . "] Ajout d'un alias mail $adresseSouhaitee -> $mailexistant pour  " . $adh->firstname . " " . $adh->lastname;

			$ipAddress = $_SERVER['REMOTE_ADDR'];
			if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
				$ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
			}

			$mesg .= "<p>Bonjour,<br />
une nouvelle demande de création d'un alias mail est en cours.</p>

<p>Vous trouverez ci-dessous les informations relatives à cette commande:</p>
<ul>
  <li>Identifiant: $login</li>
  <li>Adresse mail principale sur SOO: $mailexistant</li>
  <li>Alias demandé: $adresseSouhaitee</li>
  <li>Alias déjà existants pour ce compte : $aliasmailexistant</li>
  <li>Adresse IP du client lors de la demande : $ipAddress</li>
</ul>

<p>Normalement vous n'aviez rien à faire d'autre que vérifier que ce n'est pas un alias \"interdit\" ou \"louche\" ...</p>

\n\n";

			$mesg .= "<pre>\n--\nEnvoyé par le module sudouest sur dolibarr</pre>";

			dol_syslog("  send_mail pour informer les responsables de la création de l'alias ...");

			$mailfile = new CMailFile(
				$subject,
				$conf->notification->email_from,
				$conf->notification->email_from,
				$mesg,
				array(),
				array(),
				array(),
				'',
				'',
				0,
				$msgishtml
			);
			if (!$mailfile->sendfile()) {
				$langs->trans("errors");
				$this->error = "Erreur d'envoi mail : " . $mailfile->error;
				$retour = -1;
			}
		}

		dol_syslog("   création de l'alias dans modoboa ... (1)");

		$modo = new sudouestModoboa($db);
		$listeDesAddr = explode(',', $aliasmailexistant);
		$listeDesAddr[] = $adresseSouhaitee;
		$listeDesAddrTrim = array_map('trim', $listeDesAddr);
		if ($modo->createAlias($adresseSouhaitee, array($mailexistant)) >= 0) {
			print "<p>C'est fait, votre alias $adresseSouhaitee est maintenant opérationnel et pointe vers votre adresse principale : $mailexistant !</p>";
			print "<p><a href=\"index.php\">Retourner à l'accueil du module</a></p>";
			dol_syslog("   mise à jour du compte pour associer l'adresse modoboa ...");
			$adh->array_options["options_mailsooalias"] = implode(',', $listeDesAddrTrim);
			$adh->update($user, 1);
		} else {
			print "<p>Erreur de création de l'alias sur Modoboa !!!</p>";
		}
	} else {
		print_form_ouvertureAliasSOO();
	}
}

llxFooter();

$db->close();
