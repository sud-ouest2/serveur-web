<?php
/*
 * Copyright (C) 2017      Eric Seigne        <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 *  \file       htdocs/sudouest/action/hebergerlistes.php
 *  \ingroup    sudouest
 *  \brief      Home page of sudouest
 */

require '../../main.inc.php';
require 'common.php';
require_once 'Net/DNS2.php';

$action=GETPOST('action','alpha');

if (!$user->rights->sudouest->actions->lire)
  accessforbidden();

/*
 * View
 */

function verificationErreursDNS($adresse) {
  global $conf;
  $error  = 0;
  $errmsg = "";

  //Quand on est pressé d'ajouter le compte de listes de diff ...
  //return 0;
  
  //l'ip du serveur du client
  $h = gethostbyname($adresse);

  //notre ip (beurk on passe par les dns "rapides" de google, vraiment ?
  //j'ai des pb lorsqu'on utilise nos serveurs ... a chercher pourquoi !
  $resolver = new Net_DNS2_Resolver(); //array('nameservers' => array('8.8.8.8','1.1.1.1')));
  //pour SOO on passe par DNS2 pour avoir toutes les resolutions et pas que celle de /etc/hosts !
  $resp = $resolver->query($conf->global->SOO2_URI_MAILINGLISTES);
  $match = 0;
  for($i = 0; $i < count($resp->answer) && $match == 0; $i++) {
    $hSOO = $resp->answer[$i]->address;
    if($h == $hSOO) {
      $match = 1;
    }
  }

  //print_r($hSOO);
  if($match == 0) {
    $error++;
    $errmsg .= "L'entrée DNS $adresse ne pointe pas sur notre infrastructure ($h | $hSOO) ... merci de <a href=\"https://sud-ouest2.org/configuration-de-votre-domaine-pour-heberger-vos-listes-de-diffusions-chez-nous/\">suivre la doc</a><br />";
  }

  //Vérification MX
  getmxrr ($adresse , $mxhosts);

  if($mxhosts[0] == "" || ($mxhosts[0] != $conf->global->SOO2_MX_IN && $mxhosts[0] != $conf->global->SOO2_MX_IN_BACKUP)) {
    $error++;
    $errmsg .= "Le champ MX de $adresse ne pointe pas sur notre infrastructure (" . $mxhosts[0] . " | " . $conf->global->SOO2_MX_IN . ") ...merci de <a href=\"https://sud-ouest2.org/configuration-de-votre-domaine-pour-heberger-vos-listes-de-diffusions-chez-nous/\">suivre la doc</a><br />";
  }
  //listes IN TXT "v=spf1 a mx include:sud-ouest2.org -all"
  if($error > 0) {
    dol_htmloutput_errors($errmsg);
    print_form_ouvertureListeHebergeSOO();
  }
  return $error;
}

function print_form_ouvertureListeHebergeSOO() {
  global $db,$conf,$langs;
  
  print '<form action="'.$_SERVER["PHP_SELF"].'" method="POST" name="ouvrirListes">'."\n";
  print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'" / >';
  print '<input type="hidden" name="entity" value="'.$entity.'" />';
  print '<input type="hidden" name="action" value="ouvrirListes" />';
  
  print "    <div style=\"text-align: left; background: #eee; padding: 5px; margin: 5px;\">
	<div>
          <br/>&nbsp;&nbsp;<b>Vous voulez faire héberger vos listes de diffusions sur notre infrastructure</b>\n";
  print estimationTarifsSympa();
  print "	     <div style=\"border-left: 3px dotted #000000; margin-left: 10px; margin-bottom: 15px;\">
		<p class=\"infos\" style=\"margin: 8px 0 0 15px;\">
			Exemple: Vous possédez le nom de domaine <b>hiphiphiphoura.fr</b> et vous souhaitez créer des listes de diffusions <b>adherents@listes.hiphiphiphoura.fr</b> ... <b>encadrants@listes.hiphiphiphoura.fr</b> pour les membres de votre Association, pour vous ou pour votre entourage.
		</p>
	      	<p style=\"margin:8px 0 0 15px;\">
		        Comme expliqué <a href=\"/hebergement-de-mailing-lists-listes-de-discussion/\">dans la documentation</a>, vous vous engagez à créer un sous domaine <b>listes.votredomaine.tld</b>. Quel est votre nom de domaine ? <input class=\"forms\" type=\"text\" name=\"votreDomaineMail\" size=\"15\" maxlength=\"35\" value=\"\" />
		</p>
	     </div>
	     <div>
	      	<p style=\"margin:30px 0 0 0;\"><b>&#9670; Quel sera votre type d'utilisation ?</b> \n";
  
  //On cherche tous les services dont le nom commence par domainmail et on fait une liste ...
  //erics2
  $paymenttermstatic=new PaymentTerm($db);
  

  $sql = "SELECT * FROM ".MAIN_DB_PREFIX."product WHERE ref LIKE 'sympa%' ORDER BY ref ASC";
  $res = $db->query($sql);
  if ($res)
    {
      print "<select class=\"flat maxwidth200\" name=\"domainmailQuota\" id=\"space_id\">\n";
      $record = array();
      while ($record = $db->fetch_array($res))
	{
	  //astuce, on a besoin du quota pour creer la boite, la nomenclature des services est du genre domainmail0512mo
	  //donc on garde que les chiffres pour avoir le quota et on a besoin de l'identifiant pour la création de la facture
	  //on a donc refid:quota
	  $id  = $record["rowid"] . ":" . preg_replace('/\D/', '', $record["ref"]);
	  $txt = $record["label"];
	  print "<option value=\"" . $id . "\">" . $txt . "</option>";
	}
      print "</select>\n";
    }
  
  print " au <b>total</b> pour toutes les listes de diffusions</p>
	     </div>

	     <div>
      	     <p style=\"margin:30px 0 0 0;\"><b>&#9670; A quel tarif estimez-vous cette prestation ?</b></p>
	     <p style=\"margin:8px 0 8px 25px;\">&#x25BA; Tarif de l'hébergement: <input class=\"forms\" style=\"text-align:center\" type=\"text\" name=\"estimationTarif\" size=\"3\" maxlength=\"6\" value=\"1\" /> €uros par mois</p>

<p style=\"margin:8px 0 0 25px;\">&#x25BA; Périodicité des versements: <select class=\"flat maxwidth100\" name=\"periodicite\" id=\"period_id\"><option value=\"12\">Annuelle</option><option value=\"6\">Semestrielle</option></select></p>

<p style=\"margin:8px 0 0 25px;\">&#x25BA;  Mode de règlement envisagé: <select class=\"flat maxwidth100\" name=\"paiement\" id=\"paiement_id\"><option value=\"paypal\">Paypal / CB</option><option value=\"virement\">Virement bancaire</option><option value=\"cheque\">Chèque</option></select></p>
	     </div>

	     <div style=\"text-align:center\">
	     <input type=\"submit\" value=\"Demander la création de l'hébergement des listes de diffusions\">
	     </div>	     
	</div>
    </div>\n";
  print '</form>';
  //<option value=\"3\">Trimestrielle</option><option value=\"1\">Mensuelle</option>
}


llxHeader('',"Console de gestion utilisateur pour SudOuest","");

$form=new Form($db);
$companystatic=new Societe($db);
$contactstatic=new Contact($db);
$adh=new Adherent($db);
$adh->fetch_login($user->login);

//print_r();

// Show navigation bar
if (empty($action))
{
  print "<p>Bienvenue dans votre console de <b>gestion administrative</b> de votre compte.</p>";
  print "<a href=\"index.php\">Retourner à l'accueil du module</a>";
}
if ($action == "ouvrirListes")
{
  $existant = $adh->array_options["options_sympa"];
  if($existant) {
    print "<p>Vous avez déjà un domaine personnel hébergé sur les serveurs de l'association : $existant</p>";
    print "<p>Vous pouvez consulter la documentation pour savoir comment l'utiliser ...</p>";
  }
  //Verification si l'entrée du domaine est faite ...
  else if(GETPOST("votreDomaineMail")) {
    $adresse = trim(strtolower(GETPOST("votreDomaineMail")));
    if(substr($adresse,0,6) != "listes") {
      $domaineSeul = $adresse;
      $adresseSouhaitee = "listes.$adresse";
    }
    else {
      $domaineSeul = substr($adresse,7);
      $adresseSouhaitee = $adresse;
    }
    if(verificationErreursDNS($adresseSouhaitee) == 0) {
      //a priori on a rien a faire ... on expedie un mail à listmaster avec le lien ?

      //Tout est ok pour créer la facture et ouvrir le compte ...
      dol_syslog(" début de la création de la facture");
      //Creation de la facture
      $invoice=new Facture($db);
      $customer=$companystatic;
	
      if (! $error)
	{
	  if (! ($adh->fk_soc > 0))
	    {
	      $langs->load("errors");
	      $errmsg=$langs->trans("ErrorMemberNotLinkedToAThirpartyLinkOrCreateFirst");
	      $error++;
	    }
	}
      if (! $error)
	{
	  $result=$customer->fetch($adh->fk_soc);
	  if ($result <= 0)
	    {
	      $errmsg=$customer->error;
	      $errmsgs=$acct->errors;
	      $error++;
	    }
	}
      
      if (! $error)
	{
	  dol_syslog("   brouillon de facture");
	  // Create draft invoice
	  $invoice->type= Facture::TYPE_STANDARD;
	  $invoice->cond_reglement_id=$customer->cond_reglement_id;
	  if (empty($invoice->cond_reglement_id))
	    {
	      $paymenttermstatic=new PaymentTerm($db);
	      $invoice->cond_reglement_id=$paymenttermstatic->getDefaultId();
	      if (empty($invoice->cond_reglement_id))
		{
		  $error++;
		  $errmsg='ErrorNoPaymentTermRECEPFound';
		}
	    }
	  dol_syslog("   pour adhérent dont la société rattachée est fk_soc:" .$adh->fk_soc);
	  $invoice->socid=$adh->fk_soc;
	  
	  $datesubscription=date("Y-m-d");
	  dol_syslog("   à la date du:" .$datesubscription);
	  $invoice->date=$datesubscription;
	  
	  $result=$invoice->create($user);
	  if ($result <= 0)
	    {
	      $errmsg=$invoice->error;
	      $errmsgs=$invoice->errors;
	      $error++;
	    }
	}
      
      if (! $error)
	{
	  // Add line to draft invoice
	  $idprodsubscription=explode(":",GETPOST("domainmailQuota"))[0];
	  //ici on utilise les codes produits du formulaire expédié (erics)
	  $vattouse=get_default_tva($mysoc, $mysoc, $idprodsubscription);
	  //print xx".$vattouse." - ".$mysoc." - ".$customer;exit;
	  $label = "Hébergement listes de diffusions sur le domaine ($adresseSouhaitee)";
	  $dateend = mktime(0, 0, 0, date("m") + GETPOST("periodicite"),   date("d"),   date("Y"));
	  $datesubend=date("Y-m-d", $dateend);
	  $total=GETPOST("estimationTarif")*GETPOST("periodicite");
	  dol_syslog("   ajout d'une ligne sur la facture : $label ref($idprodsubscription) pour la période du $datesubscription au $datesubend au prix libre de $total ...");
	  
	  $result=$invoice->addline($label,GETPOST("estimationTarif"),GETPOST("periodicite"),$vattouse,0,0,$idprodsubscription,0,$datesubscription,$datesubend,0,0,'','TTC',GETPOST("estimationTarif"),1);
	  if ($result <= 0)
	    {
	      $errmsg=$invoice->error;
	      $error++;
	    }
	  //On ajoute une ligne "à zéro" qui indique le nom du domaine hébergé ... c'est mieux :)
	  $result=$invoice->addline($label,0,0,$vattouse,0,0,'',0,'','',0,0,'','TTC',0,1);
	  if ($result <= 0)
	    {
	      $errmsg=$invoice->error;
	      $error++;
	    }
	}
      
      if (! $error)
	{
	  // Validate invoice ... sauf que non, il faut la transformer en facture recurente pour ensuite avoir la facturation automatique
	  // qui sera en place ...
	  /*
	    dol_syslog("   validation de la facture ...");
	    $result=$invoice->validate($user);
	    if ($result <= 0)
	    {
	    $errmsg=$invoice->error;
	    $errmsgs=$invoice->errors;
	    $error++;
	    }
	  */
	  
	  //On envoie plutot un mail aux administrateurs pour leur expliquer ce qu'il faut faire ...
	  global $conf,$langs;
	  global $dolibarr_main_url_root;
	    
	  require_once DOL_DOCUMENT_ROOT.'/core/class/CMailFile.class.php';
	  
	  $msgishtml=1;
	  
	  // Define $msg
	  $mesg = '';
	  
	  $subject = "[" . $conf->global->MAIN_INFO_SOCIETE_NOM . "] Mise en place d'une facture automatique pour " . $adh->firstname . " " . $adh->lastname;
	  
	  $factureuri = $dolibarr_main_url_root . "/compta/facture.php?facid=" . $invoice->id;


	  $ipAddress = $_SERVER['REMOTE_ADDR'];
	  if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	    $ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
	  }
	  
	  $mesg.= "<p>Bonjour,<br />
une nouvelle commande est en cours pour un domaine hébergé et vous devez manuellement la valider pour mettre en place la facturation automatique.</p>

<p>Vous trouverez ci-dessous les informations relatives à cette commande:</p>
<ul>
  <li>Identifiant: " . $adh->firstname . " " . $adh->lastname . "</li>
  <li>Liste de diffusion: $adresseSouhaitee</li>
  <li>Périodicité de facturation (en mois): " . GETPOST("periodicite") . "</li>
  <li>Choix du tarif mensuel pour ce service: " . GETPOST("estimationTarif") . "</li>
  <li>Numéro de facture provisoire : " . $invoice->ref . "</li>
  <li>Adresse IP du client lors de la demande : $ipAddress</li>
</ul>

<p>Cette facture est à transformer en facturation automatique, veuillez donc suivre les étapes suivantes:</p>
<ul>
  <li>identifiez vous avec votre compte secrétaire ou trésorier sur <a href=\"$dolibarr_main_url_root\">$dolibarr_main_url_root</a></li>
  <li>cliquez sur l'onglet Compta/Tréso</li>
  <li>cliquez sur la facture provisoire " . $invoice->ref . " -> <a href=\"$factureuri\">$factureuri</a></li>
  <li>cliquez sur le bouton \"convertir en facture modèle\" pour en faire une source de facture automatique</li>
  <li>complétez ce forumaire :</li>
  <ul>
     <li>titre de facture automatique comme par exemple \"Hébergement liste de diffusion de $adresseSouhaitee (" . $adh->firstname . " " . $adh->lastname . ")\"</li>
     <li>configurez la récurrence : " . GETPOST("periodicite") . " mois</li>
     <li>date pour la prochaine facture : cliquer sur le lien \"maintenant\"</li>
     <li>nombre maximum de génération : 1000 (?)</li>
     <li>statut des factures générées : validée à payer</li>
     <li>puis cliquez sur le bouton créer</li>
  </ul>
  <li>c'est tout, la facture devrait se faire toute seule ... pensez quand même à vérifier demain si c'est bien le cas !</li>
  <li>normalement une facture sera générée automatiquement à échéance</li>
</ul>
\n\n";
	    
	    $mesg.= "<pre>\n--\nEnvoyé par le module sudouest sur dolibarr</pre>";
	    
	    dol_syslog("  send_mail pour demander aux responsables de faire la création de facture automatique ...");
	    
	    $mailfile = new CMailFile($subject,
				      $conf->notification->email_from,
				      $conf->notification->email_from,
				      $mesg,
				      array(),
				      array(),
				      array(),
				      '',
				      '',
				      0,
				      $msgishtml
				      );
	    
	    if (! $mailfile->sendfile())
	      {
		$langs->trans("errors");
		$this->error="Erreur d'envoi mail : " . $mailfile->error;
		$retour = -1;
	      }
	    
	}
      
      
      if (! $error)
        {
	  $db->commit();
	  dol_syslog("   facture validée ...");
        }
      else
        {
	  $db->rollback();
	  dol_syslog("   validation de facture annulée ...");
        }
      
      // Si pas d'erreur de facturation on passe a la suite : creation du compte dans modoboa
      if (! $error)
        {
	  dol_syslog("   information sympa is ok ... (1)");
	  $login = "listmaster@$domaineSeul";
	  print "<p>La création de votre système de listes de diffusions est maintenant terminée et sera <b>réellement effective d'ici 10 minutes environ</b> (le temps que l'hôte virtuel soit instancié sur le serveur).</p>";
	  print "<p>Pour administrer votre domaine vous devrez utiliser les informations affichées ci dessous (ne les perdez pas !) :</p>";
	  print "<p><ul>
<li>Adresse de connexion: <a href=\"https://$adresseSouhaitee/\">https://$adresseSouhaitee/</a></li>
<li>Identifiant mail: $login</li>
<li>Mot de passe mail: vous devrez en demander un à la première connexion</li>
</ul></p>\n";

	    //Si tout a marché on "update" l'adhérent pour qu'on sache quelle adresse sur le domaine hébergé par l'asso il a ...
	    dol_syslog("   mise à jour du compte pour associer le domaine modoboa ...");
	    $adh->array_options["options_sympa"] = $adresseSouhaitee;
	    $adh->update($user, 1);
	}
	else {
	  print "<p>Erreur de création du compte sur sympa, veuillez <a href=\"/support/\">ouvrir un ticket</a> (il faut se créer un compte si vous n'en avez pas déjà un sur la plate-forme d'assistance) et fournir le plus possible d'informations pour qu'un administrateur puisse vous aider ...</p>";
	}
    }
  }
  else {
   print_form_ouvertureListeHebergeSOO();
  }
 }
 else {
   print_form_ouvertureListeHebergeSOO();
 }

llxFooter();

$db->close();

