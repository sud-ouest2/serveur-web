<?php
/*
 * Copyright (C) 2017      Eric Seigne        <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 *  \file       htdocs/sudouest/action/index.php
 *  \ingroup    sudouest
 *  \brief      Home page of sudouest
 */

require '../../main.inc.php';
require 'common.php';

$action=GETPOST('action','alpha');

if (!$user->rights->sudouest->actions->lire)
  accessforbidden();

/*
 * View
 */

function print_form_updateMail() {
  global $db,$conf,$langs;
  global $mailsecours, $mailfacturation;
  
  print '<form action="'.$_SERVER["PHP_SELF"].'" method="POST" name="updateMail">'."\n";
  print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'" / >';
  print '<input type="hidden" name="entity" value="'.$entity.'" />';
  print '<input type="hidden" name="action" value="updateMail" />';
  
  print "
    <div style=\"text-align: left; background: #eee; padding: 5px; margin: 5px;\">
	<div>
	     <br/>&nbsp;&nbsp;<b>- Modifier votre adresses mail de facturation</b>\n";
  print "
	     <div style=\"border-left: 3px dotted #000000; margin-left: 10px; margin-bottom: 15px;\">
		<p class=\"infos\" style=\"margin: 8px 0 0 15px;\">
			Vous désirez recevoir vos factures sur l'adresse mail suivante: 
		</p>
	      	<p style=\"margin: 0 0 0 30px;\">
			&#x2514; <input class=\"forms\" type=\"text\" name=\"adresseFacturation\" size=\"40\" maxlength=\"50\" value=\"$mailfacturation\" />
		</p>
	     </div>
	     <div style=\"border-left: 3px dotted #000000; margin-left: 10px; margin-bottom: 15px;\">
		<p class=\"infos\" style=\"margin: 8px 0 0 15px;\">
			En cas de perte de mot de passe votre adresse de secours est (si vous voulez la modifier envoyez un mail à secretaires@listes.sud-ouest.org): 
		</p>
	      	<p style=\"margin: 0 0 0 30px;\">
			&#x2514; <input class=\"forms\" style=\"background-color:#eee; border:0;\" type=\"text\" name=\"adresseSecours\" size=\"40\" maxlength=\"25\" value=\"$mailsecours\" readonly/>
		</p>
	     </div>
	     <div style=\"text-align:center\">
	     <input type=\"submit\" value=\" Mettre à jour votre adresse \">
	     </div>	     
    </div>\n";
  print '</form>';
}


llxHeader('',"Console de gestion utilisateur pour SudOuest","");

$form=new Form($db);
$companystatic=new Societe($db);
$contactstatic=new Contact($db);
$adh=new Adherent($db);
$adh->fetch_login($user->login);

//print_r($adh);

print "<p>Bienvenue dans votre console de <b>gestion administrative</b> de votre compte.</p>";
$mailsecours = trim($adh->email);

$object = new Adherent($db);
$customer=$companystatic;
$result=$customer->fetch($adh->fk_soc);
$mailfacturation=$customer->email;
if($mailsecours == "") {
  $mailsecours = $mailfacturation;
}

// Show navigation bar
if (empty($action))
{
  print "<a href=\"index.php\">Retourner à l'accueil du module</a>";
}
if ($action == "updateMail")
{
  //Verification adresse mail dispo
  if(trim(GETPOST("adresseFacturation"))) {
    dol_syslog(" début de la mise a jour de l'adresse de facturation");
    $customer=$companystatic;
    $result=$customer->fetch($adh->fk_soc);
    //    print "adresse mail actuelle : " . $customer->email . " et avant $mailfacturation";
    if($customer->email == $mailfacturation) {
      $customer->email = GETPOST("adresseFacturation");
      $error = $customer->update($adh->fk_soc);
      print "<p>Modification de votre adresse mail de facturation terminée, vous recevre maintenant les factures à l'adresse suivante <b>" . GETPOST("adresseFacturation") . "</b></p>" ;
      print "<p><a href=\"index.php\">Retourner à l'accueil du module</a></p>";
    }
  }
  else {
    print_form_updateMail();
  }  
}

llxFooter();

$db->close();

