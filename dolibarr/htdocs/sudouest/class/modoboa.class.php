<?php
/*
   * Copyright (C) 2017      Eric Seigne        <eric.seigne@cap-rel.fr>
   *
   * This program is free software; you can redistribute it and/or modify
   * it under the terms of the GNU General Public License as published by
   * the Free Software Foundation; either version 3 of the License, or
   * (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   * GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with this program. If not, see <http://www.gnu.org/licenses/>.
   */

/**
 *      \defgroup   member     Module sudouest
 *      \brief      Module to manage sudouest services
 *	  \file       htdocs/core/modules/modSudouest.class.php
 *      \ingroup    sudouest
 *      \brief      File descriptor or module Sudouest
 */

require_once DOL_DOCUMENT_ROOT . '/core/lib/security2.lib.php';
/**
 *  Class
 */
class sudouestModoboa
{
  function __construct($db)
  {
    global $conf;

    $this->mailErrorsTo = $conf->global->SOO2_MAIL_ERRORSTO;

    self::resetHeader();
    $this->crl = curl_init();

    //ne pas verifier le https
    curl_setopt($this->crl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($this->crl, CURLOPT_SSL_VERIFYPEER, false);
    // Will return the response, if false it print the response
    curl_setopt($this->crl, CURLOPT_RETURNTRANSFER, true);
  }

  function resetHeader()
  {
    global $conf;
    unset($this->headr);
    $this->headr = array();
    $this->headr[] = 'Content-type: application/json';
    $this->headr[] = 'Authorization:  Token ' . $conf->global->SOO2_MODOBOA_API_TOKEN;
  }

  function curlErrorHttp()
  {
    $error = 0; //no error by default
    $info = curl_getinfo($this->crl);
    if (substr($info["http_code"], 0, 1) != "2") {
      dol_syslog("   erreur de code de retour HTTP : " . $info["http_code"]);
      $error = 1;
    }
    return $error;
  }

  //pour envoyer un mail d'info à l'équipe d'astreinte ...
  function mailError($message)
  {
    $sujet = "[soo2] alerte erreur ...";
    mail($this->mailErrorsTo, $sujet, $message);
  }

  function testMail($mail)
  {
    global $conf;
    $this->headr[] = 'Content-length: 0';

    curl_setopt($this->crl, CURLOPT_URL, $conf->global->SOO2_MODOBOA_API_URL . "/accounts/exists/?email=$mail");
    curl_setopt($this->crl, CURLOPT_HTTPHEADER, $this->headr);

    $rest = curl_exec($this->crl);
    if ($this->curlErrorHttp() || $rest === false) {
      // throw new Exception('Curl error: ' . curl_error($crl));
      //print_r('Curl error: ' . curl_error($this->crl));
    }
    $retour = json_decode($rest);
    curl_close($this->crl);
    return $retour->exists;
  }

  function testDomaine($domaine)
  {
    global $conf;
    $retour = 0;
    $this->headr[] = 'Content-length: 0';

    curl_setopt($this->crl, CURLOPT_URL, $conf->global->SOO2_MODOBOA_API_URL . "/domains/");
    curl_setopt($this->crl, CURLOPT_HTTPHEADER, $this->headr);

    $rest = curl_exec($this->crl);
    if ($this->curlErrorHttp() || $rest === false) {
      // throw new Exception('Curl error: ' . curl_error($crl));
      //print_r('Curl error: ' . curl_error($this->crl));
      $retour = -1;
    } else {
      $tabRes = json_decode($rest);
      for ($i = 0; ($i < count($tabRes)) && ($retour == 0); $i++) {
        if ($tabRes[$i]->name == $domaine) {
          $retour = 1;
        }
      }
    }
    curl_close($this->crl);
    return $retour;
  }


  function createMail($login, $firstname, $lastname, $passwd, $email, $domaine, $quota, $role)
  {
    global $conf;
    $retour = 0;
    // creation du compte ici ... mais il faut le mot de passe en clair !

    dol_syslog("   création du compte dans modoboa $login : $firstname : $lastname : $passwd : $email : $domaine : $quota");
    if ($passwd == "") {
      $passwd = getRandomPassword(false) . "A2:8Z";
    }
    //post pour creation compte
    $data = array(
      "role" => $role,
      "username" => $login,
      "first_name" => $firstname,
      "last_name" => $lastname,
      "password" => $passwd,
      "secondary_email" => $email,
      "language" => "fr",
      "is_active" => true,
      "master_user" => 0,
      "mailbox" => array(
        "full_address" => "$login",
        "use_domain_quota" => 0,
        "quota" => "$quota"
      ),
      "phone_number" => ""
    );

    //bug corrige avril 2019
    //Si on ajoute un revendeur par exemple il faut lui donner les droits sur son domaine des le depart
    if ($domaine != "") {
      $data["domains"] = array($domaine);
    }


    $data_string = json_encode($data);
    dol_syslog("   json request : $data_string");

    $this->headr[] = 'Content-length: ' . strlen($data_string);

    curl_setopt($this->crl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($this->crl, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($this->crl, CURLOPT_URL, $conf->global->SOO2_MODOBOA_API_URL . "/accounts/");
    curl_setopt($this->crl, CURLOPT_HTTPHEADER, $this->headr);

    $rest = curl_exec($this->crl);
    if ($this->curlErrorHttp() || $rest === false) {
      dol_syslog("   erreur de création du compte dans modoboa $login ! Code erreur curl : " . curl_error($this->crl));
      $jsondec = json_decode($rest);
      dol_syslog("   retour REST : $rest");
      //throw new Exception('Curl error: ' . curl_error($this->crl));
      //print_r('Curl error: ' . curl_error($crl));

      $m = "Erreur de creation de compte dans Modoboa :
login: $login
prenom: $firstname
nom: $lastname
pass: $passwd
email: $email
Domaine: $domaine
Quota: $quota
Role: $role

Code de retour de Modoboa via la requete REST : " . $rest . "

--
Envoyé depuis modoboa.class.php\n";

      $this->mailError($m);

      $retour = -1;
    } else {
      dol_syslog("   création du compte dans modoboa $login OK");
      $jsondec = json_decode($rest);
      //print_r($jsondec);
      /*
	if($jsondec["is_active"]) {
	dol_syslog("   création du compte dans modoboa $login OK et ACTIF !");
	}
      */

      $retour = 1;
    }

    curl_close($this->crl);
    return $retour;
  }


  function createDomaine($domaine, $quota)
  {
    global $conf;
    $retour = 0;
    // creation du compte ici ... mais il faut le mot de passe en clair !

    dol_syslog("   création du domaine dans modoboa $domaine : $quota");

    //post pour creation compte
    $data = array(
      "name" => $domaine,
      "quota" => $quota,
      "default_mailbox_quota" => 0,
    );
    $data_string = json_encode($data);

    $this->headr[] = 'Content-length: ' . strlen($data_string);

    curl_setopt($this->crl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($this->crl, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($this->crl, CURLOPT_URL, $conf->global->SOO2_MODOBOA_API_URL . "/domains/");
    curl_setopt($this->crl, CURLOPT_HTTPHEADER, $this->headr);

    $rest = curl_exec($this->crl);
    if ($this->curlErrorHttp() || $rest === false) {
      dol_syslog("   erreur de création du domaine dans modoboa $domaine ! Erreur Curl : " . curl_error($this->crl));

      $m = "Erreur de creation de domaine dans Modoboa :
Domaine: $domaine
Quota: $quota

Code de retour de Modoboa via la requete REST : " . curl_error($this->crl) . "

--
Envoyé depuis modoboa.class.php\n";

      $this->mailError($m);


      //throw new Exception('Curl error: ' . curl_error($this->crl));
      //print_r('Curl error: ' . curl_error($crl));
      $retour = -1;
    } else {
      dol_syslog("   création du domaine dans modoboa $domaine OK");
      $jsondec = json_decode($rest);
      //print_r($jsondec);
      /*
	if($jsondec["is_active"]) {
	dol_syslog("   création du compte dans modoboa $login OK et ACTIF !");
	}
      */
      $retour = 1;
    }

    curl_close($this->crl);
    return $retour;
  }

  //recipients : array
  function createAlias($alias, $recipients)
  {
    global $conf;
    $retour = 0;
    // creation ce l'alias ici

    dol_syslog("   création de l'alias dans modoboa $alias : $recipients");

    //post pour creation de l'alias
    $data = array(
      "address" => $alias,
      "internal" => 0,
      "enabled" => true,
      "recipients" => $recipients
    );

    $data_string = json_encode($data);

    $this->headr[] = 'Content-length: ' . strlen($data_string);

    curl_setopt($this->crl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($this->crl, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($this->crl, CURLOPT_URL, $conf->global->SOO2_MODOBOA_API_URL . "/aliases/");
    curl_setopt($this->crl, CURLOPT_HTTPHEADER, $this->headr);

    $rest = curl_exec($this->crl);
    if ($this->curlErrorHttp() || $rest === false) {
      dol_syslog("   erreur de création de l'alias interne $alias dans modoboa, ! Erreur Curl : " . curl_error($this->crl));
      $jsondec = json_decode($rest);
      print "<pre>\n";
      print_r($data);
      print_r($jsondec);
      print "</pre>\n";
      $retour = -1;
    } else {
      dol_syslog("   création de l'alias externe $alias dans modoboa réussie !");
      $jsondec = json_decode($rest);
      $retour = 1;

      //print_r($jsondec);
      /*
	if($jsondec["is_active"]) {
	dol_syslog("   création du compte dans modoboa $login OK et ACTIF !");
	}
      */
    }

    curl_close($this->crl);
    return $retour;
  }

  function searchUserID($login)
  {
    global $conf;
    dol_syslog("    searchUserID $login ...");
    //retcupere le user id modoboa correspondant a l'adresse email

    self::resetHeader();
    $this->headr[] = 'Content-length: 0';
    $this->headr[] = 'Connection: Keep-Alive';

    //Recuperation des comptes un par un ///api/v1/accounts/?search=$adresse
    curl_setopt($this->crl, CURLOPT_URL, $conf->global->SOO2_MODOBOA_API_URL . "/accounts/?search=$login");

    curl_setopt($this->crl, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($this->crl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($this->crl, CURLOPT_HEADER, 0);
    curl_setopt($this->crl, CURLOPT_HTTPHEADER, $this->headr);
    curl_setopt($this->crl, CURLOPT_AUTOREFERER, true);
    curl_setopt($this->crl, CURLOPT_FOLLOWLOCATION, true);

    $rest = curl_exec($this->crl);
    $info = curl_getinfo($this->crl);

    $inf = curl_getinfo($this->crl);
    $tabRes = json_decode($rest);

    if (substr($info["http_code"], 0, 1) != "2") {
      dol_syslog("     searchUserID erreur de code de retour HTTP : " . $info["http_code"]);
    } else {
      if ($rest === false) {
        // throw new Exception('Curl error: ' . curl_error($crl));
        dol_syslog('  Curl error: ' . curl_error($this->crl));
      } else {
        $id = $tabRes[0]->pk;
        $login = $tabRes[0]->username;
      }
    }
    //surtout pas on reutilise
    //curl_close($this->crl);
    return $id;
  }

  function getUser($login)
  {
    global $conf;
    dol_syslog("  getUser $login ...");
    //retcupere le user id modoboa correspondant a l'adresse email

    self::resetHeader();
    $this->headr[] = 'Content-length: 0';
    $this->headr[] = 'Connection: Keep-Alive';

    //Recuperation des comptes un par un ///api/v1/accounts/?search=$adresse
    curl_setopt($this->crl, CURLOPT_URL, $conf->global->SOO2_MODOBOA_API_URL . "/accounts/$login/");

    curl_setopt($this->crl, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($this->crl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($this->crl, CURLOPT_HEADER, 0);
    curl_setopt($this->crl, CURLOPT_HTTPHEADER, $this->headr);
    curl_setopt($this->crl, CURLOPT_AUTOREFERER, true);
    curl_setopt($this->crl, CURLOPT_FOLLOWLOCATION, true);

    $rest = curl_exec($this->crl);
    $info = curl_getinfo($this->crl);

    $inf = curl_getinfo($this->crl);
    $tabRes = json_decode($rest);

    if (substr($info["http_code"], 0, 1) != "2") {
      dol_syslog("    getUser erreur de code de retour HTTP : " . $info["http_code"]);
    } else {
      if ($rest === false) {
        // throw new Exception('Curl error: ' . curl_error($crl));
        dol_syslog('  Curl error: ' . curl_error($this->crl));
      } else {
        dol_syslog('  getUser OK');
      }
    }
    //surtout pas, on le reutilise
    //curl_close($this->crl);
    return;
  }

  function enableDisableMail($login, $enable)
  {
    global $conf;
    $retour = 0;
    // desactivation du compte ici
    dol_syslog("   desactivation du compte dans modoboa $login");
    $id = self::searchUserID($login);

    dol_syslog("   desactivation du compte dans modoboa $login -> Modoboa user ID: $id");
    if ($id) {
      //Ensuite on recupere l'utilisateur dans modoboa
      self::getUser($id);

      //Et enfin on peu l'updater
      $data = array(
        "pk" => $id,
        "username" => "$login",
        "is_active" => $enable,
        "master_user" => false,
        "mailbox" => array(
          "full_address" => "$login",
          "use_domain_quota" => false,
          "quota" => 64
        ),
        "role" => "SimpleUsers"
      );

      $data_string = json_encode($data);
      dol_syslog("   json request : $data_string");

      self::resetHeader();
      $this->headr[] = 'Connection: Keep-Alive';
      $this->headr[] = 'X-HTTP-Method-Override: PUT';
      $this->headr[] = 'Content-length: ' . strlen($data_string);

      $referer = $conf->global->SOO2_MODOBOA_API_URL . "/accounts/$id/";
      curl_setopt($this->crl, CURLOPT_REFERER, $referer);
      curl_setopt($this->crl, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($this->crl, CURLOPT_POSTFIELDS, $data_string);
      curl_setopt($this->crl, CURLOPT_URL, $conf->global->SOO2_MODOBOA_API_URL . "/accounts/$id/");
      curl_setopt($this->crl, CURLOPT_HTTPHEADER, $this->headr);
      curl_setopt($this->crl, CURLOPT_RETURNTRANSFER, true);

      $rest = curl_exec($this->crl);
      if ($this->curlErrorHttp() || $rest === false) {
        dol_syslog("   erreur de desactivation du compte dans modoboa $login ! Code erreur curl : " . curl_error($this->crl));
        $jsondec = json_decode($rest);
        dol_syslog("   retour REST : $rest");
        //throw new Exception('Curl error: ' . curl_error($this->crl));
        //print_r('Curl error: ' . curl_error($crl));

        $m = "Erreur de desactivation du compte dans Modoboa :
login: $login

Code de retour de Modoboa via la requete REST : " . $rest . "

--
Envoyé depuis modoboa.class.php\n";

        $this->mailError($m);

        $retour = -1;
      } else {
        dol_syslog("   desactivation du compte dans modoboa $login OK");
        $jsondec = json_decode($rest);
        //print_r($jsondec);
        /*
	  if($jsondec["is_active"]) {
	  dol_syslog("   création du compte dans modoboa $login OK et ACTIF !");
	  }
	*/

        $retour = 1;
      }
    } else {
      dol_syslog("   Erreur de desactivation du compte dans modoboa : ID non recupere");
      $retour = -1;
    }

    curl_close($this->crl);
    return $retour;
  }

  function enableMail($login)
  {
    dol_syslog("   reactivation du compte dans modoboa $login");
    self::enableDisableMail($login, true);
    return;
  }

  function disableMail($login)
  {
    self::enableDisableMail($login, false);
    dol_syslog("   desactivation du compte dans modoboa $login");
    return;
  }
}
