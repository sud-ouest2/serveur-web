<?php
/*
 * Copyright (C) 2017		Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file       htdocs/public/soo/get_liste_domains.php
 *	\ingroup    soo
 *	\brief      Get all domains for soo2
 *
 */

header("Content-Type: text/plain");

define("NOLOGIN",1);		// This means this output page does not require to be logged.
define("NOCSRFCHECK",1);	// We accept to go on this page from external web site.

require '../../main.inc.php';
require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherent.class.php';
require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherent_type.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/company.lib.php';

$sql = "SELECT domaine FROM ".MAIN_DB_PREFIX."adherent_extrafields WHERE domaine is not null ORDER BY domaine";
$resql = $db->query($sql);
$result = $db->query($sql);
if ($db->num_rows($result)) {
  while($tab = $db->fetch_object($sql)) {
    if(strpos($tab->domaine,',') === false) {
      print trim($tab->domaine) . "\n";	
    }
    else {
      $exp = explode(',',$tab->domaine);
      for($i = 0; $i < count($exp); $i++) {
	print trim($exp[$i]) . "\n";
      }
    }
  }
}
?>
