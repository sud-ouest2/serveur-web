<?php
/*
 * Copyright (C) 2017		Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *	\file       htdocs/public/soo/get_liste_members.php
 *	\ingroup    soo
 *	\brief      Get all members emails for soo2
 *
 */

header("Content-Type: text/plain");

define("NOLOGIN",1);		// This means this output page does not require to be logged.
define("NOCSRFCHECK",1);	// We accept to go on this page from external web site.

require '../../main.inc.php';
require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherent.class.php';
require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherent_type.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/company.lib.php';


//Si un mail soo existe on l'utilise et sinon on passe sur le mail de secours ...
//$sql = "SELECT email,mailsoo FROM ".MAIN_DB_PREFIX."adherent a INNER JOIN ".MAIN_DB_PREFIX."adherent_extrafields b ON a.rowid=b.fk_object WHERE a.statut > 0";
$sql = "SELECT a.email as email,mailsoo,c.email as socemail FROM ".MAIN_DB_PREFIX."adherent a INNER JOIN ".MAIN_DB_PREFIX."adherent_extrafields b ON a.rowid=b.fk_object INNER JOIN ".MAIN_DB_PREFIX."societe c ON a.fk_soc=c.rowid WHERE a.statut > 0";
//print $sql;
$resql = $db->query($sql);
$result = $db->query($sql);
if ($db->num_rows($result)) {
  $liste = array();
  while($tab = $db->fetch_object($sql)) {
    if($tab->mailsoo != "") {
      $liste[] = $tab->mailsoo;
      //print $tab->mailsoo . "\n";
    }
    else {
      if($tab->socemail != "") {
	$liste[] = $tab->socemail;
      }
      else {
	if($tab->email != "") {
	  $liste[] = $tab->email;
	  //print $tab->email . "\n";
	}
      }
    }
  }
  $result = array_unique($liste);
  sort($result);
  print implode("\n",$result);
}

?>
