<?php
/* Copyright (C) 2005-2013 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2014       Marcos García       <marcosgdf@gmail.com>
 * Copyright (C) 2017       Eric Seigne		<eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  \file       htdocs/core/triggers/interface_51_modSudouestsynchro.class.php
 *  \ingroup    core
 *  \brief      File to manage triggers Sudouest
 */
require_once DOL_DOCUMENT_ROOT . '/core/triggers/dolibarrtriggers.class.php';
require_once DOL_DOCUMENT_ROOT . "/user/class/usergroup.class.php";
require_once DOL_DOCUMENT_ROOT . "/societe/class/societe.class.php";
require_once DOL_DOCUMENT_ROOT . '/adherents/class/adherent.class.php';
require_once DOL_DOCUMENT_ROOT . '/adherents/class/adherent_type.class.php';
require_once DOL_DOCUMENT_ROOT . '/compta/facture/class/facture.class.php';
require_once DOL_DOCUMENT_ROOT . '/compta/paiement/class/paiement.class.php';
require_once DOL_DOCUMENT_ROOT . '/compta/bank/class/account.class.php';
require_once DOL_DOCUMENT_ROOT . '/core/lib/functions.lib.php';

//erics
require_once DOL_DOCUMENT_ROOT . '/sudouest/class/modoboa.class.php';

/**
 *  Class of triggers for SudOuest/Sudouest module
 */
class InterfaceSudouestsynchro extends DolibarrTriggers
{
	public $family = 'core';
	public $description = "Triggers of this module allows to synchronize Sudouest/Modoboa.";
	public $version = self::VERSION_DOLIBARR;
	public $picto = 'technic';


	/* factorisation du code de creation du compte, de la société et des liens entre les objets */
	function autoCreateAccountAndOther($adherent)
	{
		global $langs;
		global $db;

		$retour = 0;
		dol_syslog(" autoCreateAccountAndOther start");

		// --------------------------------------------- CREATION DU COMPTE DOLIBARR AUTOMATIQUE
		if ($adherent->lastname == "" && $adherent->firstname == "") {
			//on a un hic ... pas la peine d'aller plus loin pour l'instant
			dol_syslog(" autoCreateAccountAndOther erreur : compte 'vide' -> corriger cf ligne 56");
			return;
		}
		//2019 on ajoute le login a la fin pour eviter les homonymes
		$nom = strtoupper($adherent->lastname) . " " . $adherent->firstname . "(" . $adherent->login . ")";

		// *********** creation du tiers ... attention si tiers deja existant ...
		dol_syslog("   Recherche si un tiers '$nom' n'existe pas déjà ...");

		/*
	    $sql = "SELECT nom FROM ".MAIN_DB_PREFIX."societe WHERE nom='" . $db->escape($nom) . "';";
	    dol_syslog("   Recherche 0 tiers '$sql'");
	    $resql = $db->query($sql);
	    dol_syslog("   Recherche A tiers '$sql'");
	    $num = $db->num_rows($resql);
	    dol_syslog("   Recherche B tiers '$sql'");
	  */
		$company = new Societe($db);

		//erics2020	  $res = $company->searchByName($db->escape($nom));
		//print "retour de la recherche : $res";
		//erics2020	  if(count($res) <= 0 ) {
		$res = $company->fetch(null, $db->escape($nom));
		//print "retour de la recherche : $res";
		if ($res <= 0) {
			//if($num <= 0 ) {
			dol_syslog("   Le tiers n'existe pas, on le créé : '$nom'");
			dol_syslog("   Creation du tiers 1 '$nom'");

			$socid = $company->create_from_member($adherent, $nom);
			if ($socid < 0) {
				$langs->load("errors");
				setEventMessages($langs->trans($company->error), null, 'errors');
				setEventMessages($company->error, $company->errors, 'errors');
			}
			//print_r($company);
			//$socid=$company->id;
		} else {
			dol_syslog("   Le tiers '$nom' existe déjà ...");
			/*
	      print "<pre>";
	      print_r($res);
	      print "</pre>";
	    */
			$socid = $company->id;
		}


		// *********** creation du compte
		$nom = $adherent->firstname . " " . $adherent->lastname;
		dol_syslog("   Création du compte '$nom' ... (0)");
		$nuser  = new User($db);

		if ($nuser->fetch('', $adherent->login) > 0) {
			//pas la peine de le creer il existe deja
			dol_syslog("     Création du compte '$nom' pas nécessaire il existe déjà !");
		} else {
			//print_r($nuser);
			dol_syslog("   Création du compte '$nom' à venir");
			$result = $nuser->create_from_member($adherent, $adherent->login);
			dol_syslog("     Création du compte '$nom' faite !");
			$db->commit();
		}
		$nuser->fetch('', $adherent->login);
		dol_syslog("     Le compte '$nom' a pour ID " . $nuser->id);

		//On passe l'adherent en identifiant "externe"
		/*
	    $adht = new Adherent($db);
	    $adht->fetch($adherent);
	  */
		$adherent->setThirdPartyId($socid);
		dol_syslog("     Association de l'adhérent avec la thirdParty $socid");

		//et pour le user c'est du SQL direct ... c'est la qu'il faut debug serieux
		$db->begin();
		$sql = "UPDATE " . MAIN_DB_PREFIX . "user";
		$sql .= " SET fk_soc = " . $socid;
		$sql .= " WHERE rowid = '" . $nuser->id . "'";
		$result = $db->query($sql);
		dol_syslog("     erics user manuel : $sql ... ");
		$db->commit();

		$adherent->setUserId($nuser->id);
		dol_syslog("     Association de l'utilisateur avec la thirdParty $socid");

		$db->begin();
		//attention hardcoded
		$result = $nuser->SetInGroup(1, $socid, 1);
		dol_syslog("     Ajout de l'utilisateur '$nom' dans le groupe adhérents (1) et compagnie " . $company->id . " hardcodé");
		$db->commit();

		//	  $adherent->societe = $nom;
		//	  $adherent->company = $nom;

		//$adherent->update(,1);
		$retour = 1;
		// --------------------------------------------- END CREATION DU COMPTE DOLI
		dol_syslog("  autoCreateAccountAndOther end");
		return $retour;
	}


	/**
	 * Function called when a Dolibarrr business event is done.
	 * All functions "runTrigger" are triggered if file is inside directory htdocs/core/triggers or htdocs/module/code/triggers (and declared)
	 *
	 * @param string		$action		Event action code
	 * @param Object		$object     Object
	 * @param User		    $user       Object user
	 * @param Translate 	$langs      Object langs
	 * @param conf		    $conf       Object conf
	 * @return int         				<0 if KO, 0 if no triggered ran, >0 if OK
	 */
	public function runTrigger($action, $object, User $user, Translate $langs, Conf $conf)
	{
		global $db;

		//        if (empty($conf->mailmanspip->enabled)) return 0;     // Module not active, we do nothing

		if (!function_exists('curl_init')) {
			dol_syslog("Warning, CURL functions not available in this PHP", LOG_WARNING);
			return 0;
		}


		//Si le paiement est ok on active le compte ...
		//plus depuis la vague de cons qui spamment ete 2018 ... desole pour les gentils
		if ($action == 'PAYPAL_PAYMENT_OK') {
			dol_syslog("Trigger '" . $this->name . "' for action '$action' launched by " . __FILE__ . ". source=" . $object->source . " ref=" . $object->ref);

			if (!empty($object->source)) {
				//dans le cas d'une inscription
				if ($object->source == 'membersubscription') {
					//require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherents.class.php';
					//$nom = $object->lastname . " " . $object->firstname;
					dol_syslog(" Plus de validation automatique de l'adhérent ref " . $object->ref . " / " . $object->payerID);

					/* commentaire ete 2018
		    $adht = new Adherent($db);
		    $adht->fetch($object->ref);

		    $result=$adht->validate($user);
		    $adht->update($user,1);
		    $db->commit();

		    $this->autoCreateAccountAndOther($adht);

*/
					//TODO : creation facture et paiement automatique ...
					dol_syslog(" Création de la facture d'adhesion ref " . $object->ref . " / " . $object->payerID);
					/*
		      $obj = new Facture($db);
		      $ret = $obj->fetch('',$object->ref);
		      if ($ret >= 0) {
		      // Add payer id
		      dol_syslog("   ajout du paiement .... " . $object->payerID);
		      $paiement = new Paiement($db);
		      $paiement->datepaye     = date("Y-m-d");
		      $paiement->amounts      = $object->FinalPaymentAmt;
		      $paiement->paiementid   = $object->payerID;
		      $paiement->note         = $object->token;
		      dol_syslog("   paiement détails : " . $object->FinalPaymentAmt . " | " . $object->payerID . " | " . $object->token);

		      $paiement_id = $paiement->create($user);
		      dol_syslog("   paiement ajouté, id " . $paiement_id);

		      //recuperation de l'identifiant du compte paypal ...
		      $sql="SELECT rowid,label FROM llx_bank_account WHERE label LIKE 'Paypal";
		      dol_syslog("   recherche de la ref du compte paypal : $sql");
		      $resql = $db->query($sql);
		      if ($resql) {
		      $obj = $db->fetch_object($resql);
		      if ($obj && $obj->rowid > 0) {
		      $paypalbankid = $obj->rowid;
		      dol_syslog("   bankid du compte paypal : $paypalbankid");

		      // Add transaction id
		      dol_syslog("   ajout de la reference de transaction sur le compte bancaire " . $object->resArray["TRANSACTIONID"]);
		      //$bank_line_id = $paiement->addPaymentToBank($user,'payment','',$accountid,$emetteur_nom,$emetteur_banque);
		      $bank_line_id = $paiement->addPaymentToBank($user,'payment','Facture ' . $object->ref , $paypalbankid, "paypal", "paypal");

		      dol_syslog("   marque la facture comme payée");
		      $fac->set_paid($user);
		      //$obj->setValueFrom('ref_int',$object->resArray["TRANSACTIONID"]);

		      $db->commit();
		      }
		      }
		      }
		      else {
		      dol_syslog("   erreur de recuperation de la facture ... ref " . $object->ref . " / " . $object->payerID);
		      }
		    */
				}
				//dans le cas du paiement d'une facture plus classique
				if ($object->source == 'invoice') {
					//require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherents.class.php';
					//$nom = $object->lastname . " " . $object->firstname;
					dol_syslog(" Validation de la facture ref " . $object->ref . " / " . $object->payerID);

					$soc = new Societe($db);
					$fac = new Facture($db);
					$ret = $fac->fetch('', $object->ref);
					if ($ret >= 0) {
						// Add payer id
						dol_syslog("   ajout du paiement .... " . $object->payerID);
						$paiement = new Paiement($db);
						$paiement->datepaye     = date("Y-m-d");
						$paiement->amounts      = $object->FinalPaymentAmt;
						$paiement->paiementid   = $object->payerID;
						$paiement->note         = $object->token;
						dol_syslog("   paiement détails : " . $object->FinalPaymentAmt . " | " . $object->payerID . " | " . $object->token);

						$paiement_id = $paiement->create($user);
						dol_syslog("   paiement ajouté, id " . $paiement_id);

						//recuperation de l'identifiant du compte paypal ...
						$sql = "SELECT rowid,label FROM llx_bank_account WHERE label LIKE 'Paypal";
						dol_syslog("   recherche de la ref du compte paypal : $sql");
						$resql = $db->query($sql);
						if ($resql) {
							$obj = $db->fetch_object($resql);
							if ($obj && $obj->rowid > 0) {
								$paypalbankid = $obj->rowid;
								dol_syslog("   bankid du compte paypal : $paypalbankid");

								// Add transaction id
								dol_syslog("   ajout de la reference de transaction sur le compte bancaire " . $object->resArray["TRANSACTIONID"]);
								//$bank_line_id = $paiement->addPaymentToBank($user,'payment','',$accountid,$emetteur_nom,$emetteur_banque);
								$bank_line_id = $paiement->addPaymentToBank($user, 'payment', 'Facture ' . $object->ref, $paypalbankid, "paypal", "paypal");

								dol_syslog("   marque la facture comme payée");
								$fac->set_paid($user);
								//$obj->setValueFrom('ref_int',$object->resArray["TRANSACTIONID"]);

								$db->commit();
							}
						}
					} else {
						dol_syslog("   erreur de recuperation de la facture ... ref " . $object->ref . " / " . $object->payerID);
					}
				}
			}
		}
		// Creation du compte -> ajout dans le groupe adhoc
		if ($action == 'USER_CREATE') {
			return 0;
		}

		// Members
		if ($action == 'MEMBER_VALIDATE' || $action == 'MEMBER_MODIFY') {
			dol_syslog("Trigger '" . $this->name . "' for action '$action' launched by " . __FILE__ . ". id=" . $object->id);

			$return = 0;

			/* ete 2018 on valide manuellement les adherents */

			$adht = new Adherent($db);
			$adht->fetch($object->ref);

			$result = $adht->validate($user);
			$adht->update($user, 1);
			$db->commit();

			/* fin de modif ete 2018 */


			$this->autoCreateAccountAndOther($object);
			// --------------------------------------------- MODOBOA START
			// activation du compte -> plus tard
			/*
	    $crl = curl_init();

	    $headr = array();
	    $headr[] = 'Content-type: application/json';
	    $headr[] = 'Authorization:  Token ' . $conf->global->SOO2_MODOBOA_API_TOKEN;

	    //ne pas verifier le https
	    curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);

	    //get pour recuperer la liste des comptes car il faut avoir l'ID pour faire un update ... très peu optimal sur le coup
	    //l'API de Modoboa !
	    //verification compte existant
	    $headr[] = 'Content-length: 0';
	    curl_setopt($crl, CURLOPT_URL, $url . "/accounts/");
	    //important si on veut recuperer le resultat plutot que l'afficher direct
	    curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($crl, CURLOPT_HTTPHEADER,$headr);

	    $rest = curl_exec($crl);
	    if ($rest === false)
	      {
		// throw new Exception('Curl error: ' . curl_error($crl));
		//print_r('Curl error: ' . curl_error($crl));
	      }
	    else {
	      $tabRes = json_decode($rest);
	      for($i = 0; $i < count($tabRes); $i++) {
		//print "***************\n";
		if($tabRes[$i]->username == $object->email) {
		  $id = $tabRes[$i]->pk;

		  //post pour activation du compte
		  $data = array( "id" => $id,
				 "username" => $object->email,
				 "role" => "SimpleUsers",
				 "language" => "fr",
				 "is_active" => true
				 );

		  $data_string = json_encode($data);
		  curl_setopt($crl, CURLOPT_CUSTOMREQUEST, "POST");
		  curl_setopt($crl, CURLOPT_POSTFIELDS, $data_string);
		  curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		  $headr[] = 'Content-length: ' . strlen($data_string);
		  curl_setopt($crl, CURLOPT_URL, $conf->global->SOO2_MODOBOA_API_URL . "/accounts/$id/");

		  curl_setopt($crl, CURLOPT_HTTPHEADER,$headr);

		  $rest = curl_exec($crl);
		  if ($rest === false) {
		    throw new Exception('Curl error: ' . curl_error($crl));
		    //print_r('Curl error: ' . curl_error($crl));
		    $return = -1;
		  }
		  else {
		    $return = 1;
		  }

		  break;
		}
		//		print "_______________\n";
	      }

	    }


	    curl_close($crl);
	    //	    print_r($rest);
	    */
			// --------------------------------------------- MODOBOA END

			return $return;
		} elseif ($action == 'MEMBER_RESILIATE' || $action == 'MEMBER_DELETE') {
			dol_syslog("Trigger '" . $this->name . "' for action '$action' launched by " . __FILE__ . ". id=" . $object->id);

			$return = 0;

			$mailexistant = $object->array_options["options_mailsoo"];
			dol_syslog("  desactive le compte $mailexistant / ");

			$modo = new sudouestModoboa($db);
			if ($modo->disableMail($mailexistant)) {
				dol_syslog("  desactivation OK  ...");
				$return = 1;
			} else {
				dol_syslog("  desactivation NOK  ...");
			}
			return $return;
		}

		return 0;
	}
}
