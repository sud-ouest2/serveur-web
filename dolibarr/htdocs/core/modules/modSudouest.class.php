<?php
/*
   * Copyright (C) 2017      Eric Seigne        <eric.seigne@cap-rel.fr>
   *
   * This program is free software; you can redistribute it and/or modify
   * it under the terms of the GNU General Public License as published by
   * the Free Software Foundation; either version 3 of the License, or
   * (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   * GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with this program. If not, see <http://www.gnu.org/licenses/>.
   */

/**
 *      \defgroup   member     Module sudouest
 *      \brief      Module to manage sudouest services
 *	\file       htdocs/core/modules/modSudouest.class.php
 *      \ingroup    sudouest
 *      \brief      File descriptor or module Sudouest
 */
include_once DOL_DOCUMENT_ROOT . '/core/modules/DolibarrModules.class.php';

/**
 *  Class to describe and enable module Adherent
 */
class modSudouest extends DolibarrModules
{

        /**
         *   Constructor. Define names, constants, directories, boxes, permissions
         *
         *   @param      DoliDB		$db      Database handler
         */
        function __construct($db)
        {
                global $conf;

                $this->db = $db;
                $this->numero = 500100;

                $this->family = "hr";
                $this->module_position = 20;
                // Module label (no space allowed), used if translation string 'ModuleXXXName' not found (where XXX is value of numeric property 'numero' of module)
                $this->name = preg_replace('/^mod/i', '', get_class($this));
                $this->description = "Management of services for <a href='https://sud-ouest2.org'>sud-ouest2.org</a> hosting";
                $this->version = 'dolibarr';                        // 'experimental' or 'dolibarr' or version
                $this->const_name = 'MAIN_MODULE_' . strtoupper($this->name);
                $this->special = 0;
                $this->picto = 'user';

                // Data directories to create when module is enabled
                //$this->dirs = array("/sudouest/temp");

                // Config pages
                //-------------
                $this->config_page_url = array("sudouest.php@sudouest");

                // Dependencies
                //------------
                $this->depends = array('modAdherent');
                //$this->requiredby = array('modMailmanSpip');
                $this->langfiles = array("members", "companies");

                // Constants
                //-----------
                $this->const = array();
                $this->const[1] = array("SOO2_MODOBOA_API_TOKEN", "chaine", "xxxxxxxxxxxxxxxxx", "Clé de l'API de programmation de Modoboa");
                $this->const[2] = array("SOO2_MODOBOA_API_URL", "chaine", "https://mail.domaine.org/api/v1", "Adresse de l'API de Modoboa");
                $this->const[3] = array("SOO2_MX_IN", "chaine", "mx.domaine.org", "Serveur MX entrant de l'infrastructure");
                $this->const[3] = array("SOO2_MX_IN_BACKUP", "chaine", "mx-backup.domaine.org", "Serveur MX entrant de l'infrastructure");
                $this->const[4] = array("SOO2_URI_MAILINGLISTES", "chaine", "listes.domaine.org", "Serveur d'hébergement des listes de diffusions");
                $this->const[5] = array("SOO2_MAIL_DOMAINS", "chaine", "sud-ouest2.org,sud-ouest.org,mailz.org,...", "Liste des noms de domaines hébergés de base par la structure séparés par des virgules");
                $this->const[6] = array("SOO2_MAIL_ERRORSTO", "chaine", "admin@sud-ouest2.org", "Adresse mail à laquelle les erreurs sont rapportées");
                $this->const[7] = array("SOO2_MAIL_ABUSE", "chaine", "abuse@sud-ouest2.org", "Adresse mail abuse à appliquer à tous les domaines hébergés");
                $this->const[8] = array("SOO2_MAIL_POSTMASTER", "chaine", "postmaster@sud-ouest2.org", "Adresse postmaster à appliquer à tous les domaines hébergés");
                $this->const[9] = array("SOO2_MAIL_WEB_URL", "chaine", "https://mail.sud-ouest2.org", "Adresse publique de l'interface web de gestion");

                // Boxes
                //-------
                //$this->boxes = array(0=>array('file'=>'box_sudouest.php','enabledbydefaulton'=>'Home'));

                // Permissions
                //------------
                $this->rights = array();
                $this->rights_class = 'sudouest';
                $r = 0;
                $this->rights[$r][0] = 50102;
                $this->rights[$r][1] = 'Accès au module';
                $this->rights[$r][3] = 1;
                $this->rights[$r][4] = 'actions';
                $this->rights[$r][5] = 'lire';

                // $this->rights[$r][0]     Id permission (unique tous modules confondus)
                // $this->rights[$r][1]     Libelle par defaut si traduction de cle "PermissionXXX" non trouvee (XXX = Id permission)
                // $this->rights[$r][2]     Non utilise
                // $this->rights[$r][3]     1=Permis par defaut, 0=Non permis par defaut
                // $this->rights[$r][4]     Niveau 1 pour nommer permission dans code
                // $this->rights[$r][5]     Niveau 2 pour nommer permission dans code


                // Menus
                //-------
                $this->menu = array();                        // List of menus to add
                $r = 0;

                $this->menu[$r] = array(
                        'fk_menu' => 0,
                        'type' => 'top',
                        'titre' => 'Sud-Ouest',
                        'mainmenu' => 'sudouest',
                        'leftmenu' => 'sudouest',
                        'url' => '/sudouest/action/index.php',
                        'langs' => 'sudouest',
                        'position' => 200,
                        'perms' => '$user->rights->sudouest->actions->lire',
                        'enabled' => '$conf->sudouest->enabled',
                        'target' => '',
                        'user' => 2
                );
                $r++;

                $this->menu[$r] = array(
                        'fk_menu' => 'fk_mainmenu=home,fk_leftmenu=admintools',
                        'type' => 'left',
                        'titre' => 'Sud-Ouest',
                        'url' => '/sudouest/action/index.php',
                        'langs' => 'sudouest',
                        'position' => 200,
                        'perms' => '$user->rights->sudouest->actions->lire',
                        'enabled' => '$conf->sudouest->enabled',
                        'target' => '',
                        'user' => 2
                );
                $r++;
        }
}
