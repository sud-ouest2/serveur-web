#!/usr/bin/env php
<?php
/*
 * Copyright (C) 2009-2012 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2017-2018 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      \file       scripts/invoices/rebuild_merge_pdf.php
 *      \ingroup    facture
 *      \brief      Script to rebuild PDF and merge PDF files into one
 */

$sapi_type = php_sapi_name();
$script_file = basename(__FILE__);
$path=dirname(__FILE__).'/';

// Test if batch mode
if (substr($sapi_type, 0, 3) == 'cgi') {
    echo "Error: You are using PHP for CGI. To execute ".$script_file." from command line, you must use PHP for CLI mode.\n";
	exit(-1);
}

// Include Dolibarr environment
require_once($path."../../htdocs/master.inc.php");
// After this $db is an opened handler to database. We close it at end of file.
require_once(DOL_DOCUMENT_ROOT."/compta/facture/class/facture.class.php");
require_once(DOL_DOCUMENT_ROOT."/core/modules/facture/modules_facture.php");
require_once(DOL_DOCUMENT_ROOT."/core/lib/date.lib.php");
require_once(DOL_DOCUMENT_ROOT.'/core/lib/invoice2.lib.php');


// Load main language strings
$langs->load("main");

// Global variables
$version=DOL_VERSION;
$error=0;



/*
 * Main
 */

@set_time_limit(0);
print "***** ".$script_file." (".$version.") pid=".dol_getmypid()." *****\n";
dol_syslog($script_file." launched with arg ".join(',',$argv));

$diroutputpdf=$conf->facture->dir_output . '/temp';
$newlangid='fr_FR';	// To force a new lang id
$filter=array('nopayment', 'date');
//$filter[]="date";
$regenerate='crabe';		// Ask regenerate (contains name of model to use)
$option='nopayment';
$fileprefix='mergedpdf';
$nbjoursretardA=$argv[1];
$nbjoursretardB=$argv[2];
$ladateA = mktime(0, 0, 0, date("n"), date("j") - $nbjoursretardA, date("Y"));
$datebeforedate=dol_stringtotime(date("Ymd",$ladateA));
$ladateB = mktime(0, 0, 0, date("n"), date("j") - $nbjoursretardB, date("Y"));
$dateafterdate=dol_stringtotime(date("Ymd",$ladateB));

echo "on essaye de recuperer que les factures de $nbjoursretard soit $datebeforedate | $dateafterdate ou " . date("j-n-Y",$ladateA) . ":" . date("j-n-Y",$ladateB) . " ...\n";

print 'Rebuild PDF for invoices with at least one payment between '.dol_print_date($dateafterdate,'day','gmt')." and ".dol_print_date($datebeforedate,'day','gmt').".\n";

// Define SQL and SQL request to select invoices
// Use $filter, $dateafterdate, datebeforedate, $paymentdateafter, $paymentdatebefore
$result=rebuild_merge_pdf($db, $langs, $conf, $diroutputpdf, $newlangid, $filter, $dateafterdate, $datebeforedate, $paymentdateafter, $paymentdatebefore, 1, $regenerate, $option, $paymentonbankid, $thirdpartiesid, $fileprefix);

// -------------------- END OF YOUR CODE --------------------

if ($result >= 0)
{
	$error=0;
	print '--- end ok'."\n";
}
else
{
	$error=$result;
	print '--- end error code='.$error."\n";
}

$db->close();

exit($error);



/**
 * Show usage of script
 *
 * @return unknown
 */
function usage()
{
  global $script_file;
  
  print "Rebuild PDF files for some invoices and merge PDF files into one.\n";
  print "\n";
  print "To build/merge PDF for invoices in a date range:\n";
  print "  Usage:   ".$script_file." filter=date dateafter datebefore\n";
  print "To build/merge PDF for invoices with at least one payment in a date range:\n";
  print "  Usage:   ".$script_file." filter=payments dateafter datebefore\n";
  print "To build/merge PDF for invoices with at least one payment onto a bank account:\n";
  print "  Usage:   ".$script_file." filter=bank bankref\n";
  print "To build/merge PDF for all invoices, use filter=all\n";
  print "  Usage:   ".$script_file." filter=all\n";
  print "To build/merge PDF for invoices with no payments, use filter=nopayment\n";
  print "  Usage:   ".$script_file." filter=nopayment\n";
  print "To exclude credit notes, use filter=nocreditnote\n";
  print "To exclude replacement invoices, use filter=noreplacement\n";
  print "To exclude deposit invoices, use filter=nodeposit\n";
  print "To exclude some thirdparties, use filter=excludethirdparties id1,id2...\n";
  print "To limit to some thirdparties, use filter=onlythirdparties id1,id2...\n";
  print "To regenerate existing PDF, use regenerate=crabe\n";
  print "To generate invoices in a language, use lang=xx_XX\n";
  print "To set prefix of generated file name, use prefix=myfileprefix\n";
  print "\n";
  print "Example: ".$script_file." filter=payments 20080101 20081231 lang=fr_FR regenerate=crabe\n";
  print "Example: ".$script_file." filter=all lang=en_US\n";
  print "\n";
  print "Note that some filters can be cumulated.\n";
}
