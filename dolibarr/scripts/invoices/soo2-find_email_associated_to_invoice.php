#!/usr/bin/env php
<?php
/*
 * Copyright (C) 2005		Rodolphe Quiedeville <rodolphe@quiedeville.org>
 * Copyright (C) 2005-2013	Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2013		Juanjo Menent <jmenent@2byte.es>
 * Copyright (C) 2017-2018	Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      \file       scripts/invoices/email_unpaid_invoices_to_customers.php
 *      \ingroup    facture
 *      \brief      Script to find email associated to an invoice number
 */

$sapi_type = php_sapi_name();
$script_file = basename(__FILE__);
$path=dirname(__FILE__).'/';

// Test si mode batch
$sapi_type = php_sapi_name();
if (substr($sapi_type, 0, 3) == 'cgi') {
    echo "Error: You are using PHP for CGI. To execute ".$script_file." from command line, you must use PHP for CLI mode.\n";
	exit(-1);
}

$mode="confirm";
$invoiceref=str_replace(".pdf","",trim($argv[1]));

if($invoiceref == "") {
  print "\n\n\n";
  print "Erreur de lancement, veuillez lancer le script shell qui fait appel automatiquement à ce script php\n";
  print "  reference = $invoiceref\n";
  print "\n\n\n";
  exit -2;
}

require($path."../../htdocs/master.inc.php");
require_once (DOL_DOCUMENT_ROOT."/core/class/CMailFile.class.php");
require_once DOL_DOCUMENT_ROOT.'/paypal/lib/paypal.lib.php';

$langs->load('main');

// Global variables
$version=DOL_VERSION;
$error=0;

/*
 * Main
 */

@set_time_limit(0);
//print "***** ".$script_file." (".$version.") pid=".dol_getmypid()." *****\n";
dol_syslog($script_file." launched with arg ".join(',',$argv));

$now=dol_now('tzserver');
$duration_value='none';
$duration_value2='none';

$error = 0;
//print $script_file." launched with mode ".$mode." default lang=".$langs->defaultlang.(is_numeric($duration_value)?" delay=".$duration_value:"").(is_numeric($duration_value2)?" after=".$duration_value2:"")."\n";

if ($mode != 'confirm') $conf->global->MAIN_DISABLE_ALL_MAILS=1;

//Pour l'instant on envoie pas sur le mail de secours ...
$sql  = "SELECT f.fk_soc, s.email as email1, a.email as email2, ae.mailsoo as email3, ae.mailsecours as email4";
$sql .= "  FROM ".MAIN_DB_PREFIX."facture AS f";
$sql .= "  INNER JOIN ".MAIN_DB_PREFIX."societe AS s ON  f.fk_soc=s.rowid";
$sql .= "  INNER JOIN ".MAIN_DB_PREFIX."adherent AS a ON a.fk_soc=f.fk_soc";
$sql .= "  INNER JOIN ".MAIN_DB_PREFIX."adherent_extrafields AS ae ON ae.fk_object=a.rowid";
$sql .= "  AND f.ref='$invoiceref'";

//print $sql . "\n\n";

$resql=$db->query($sql);
if ($resql) {
    $num = $db->num_rows($resql);
    $i = 0;
    $obj = $db->fetch_object($resql);
    print($obj->email1);
    /*
      a voir pour plus tard si on a besoin de recuperer toutes les adresses mails rattachees
      $result = array();
      $result[$obj->email1] = $obj->email1;
      $result[$obj->email2] = $obj->email2;
      $result[$obj->email3] = $obj->email3;
      print(implode(",",$result));
    */
}

print "\n";
