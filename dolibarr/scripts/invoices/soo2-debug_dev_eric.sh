#!/bin/bash
#outil de relance automatique pour les factures impayées ...


DOLIHOME="/var/www/dolibarr"
LISTING=`tempfile -p relance`
rm -f ${LISTING}
#LISTING="/tmp/filexfEgCcTEST"
HOMESCRIPT="$(dirname "$0")"
cd "${HOMESCRIPT}"

function lecho() {
  echo "[soo2/factureImpayee] $1"
}

lecho "verification d'usage avant tout ..."

#on fait une petite verif
php ${DOLIHOME}/scripts/eric/soo2-corrige_affectation_user_societe.php

lecho "generation des PDF pour les factures impayees dans ${LISTING}..."
#on genere tous les PDF des factures impayées et on recupere les noms de ces factures dans LISTING
#le 30 c'est le nombre de jours de "retard"
#le 6000 c'est le nombre max de jours de retard, au dela de 6000 jours on ne relance plus :)
${DOLIHOME}/scripts/invoices/soo2-rebuild_all_invoices.php 30 6000 | grep "Build PDF for invoice" | sed s/"Build PDF for invoice \(.*\) - Lang = fr_FR"/"\1"/ > ${LISTING}

for facture in `cat ${LISTING}`
do
    lecho "il faut faire une relance pour $facture"
    FICFAC="${DOLIHOME}/documents/facture/${facture}/${facture}.pdf"
    if [ -f ${FICFAC} ]; then
	maildest=`pdfgrep -P ".*boite.* (.*@.*) .*" ${FICFAC} | sed  s/".* \(.*@.*\)0%.*"/"\1"/g | xargs`
	if [ -z "${maildest}" ]; then
	    maildest=`${DOLIHOME}/scripts/invoices/soo2-find_email_associated_to_invoice.php ${facture}`
	fi
	if [ ! -z "${maildest}" ]; then
	    lecho "il faut envoyer une relance a ${maildest} pour ${FICFAC} ..."
	    (
		cd ${DOLIHOME}/documents/facture/${facture}
		${DOLIHOME}/scripts/invoices/soo2-email_relance_1mois_et_plus.php ${facture} ${maildest} ${facture}.pdf
		#lecho "sleep 1...(debug)"
		#sleep 1
	    )
	fi
    else
	lecho "Erreur grave, le fichier ${FICFAC} n'est pas présent !!!!"
    fi
done

chown www-data:www-data ${DOLIHOME}/documents/facture -R
