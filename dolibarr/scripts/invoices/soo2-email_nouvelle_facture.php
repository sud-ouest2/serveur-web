#!/usr/bin/env php
<?php
/*
 * Copyright (C) 2005		Rodolphe Quiedeville <rodolphe@quiedeville.org>
 * Copyright (C) 2005-2013	Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2013		Juanjo Menent <jmenent@2byte.es>
 * Copyright (C) 2017-2018	Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      \file       scripts/invoices/email_unpaid_invoices_to_customers.php
 *      \ingroup    facture
 *      \brief      Script to send a mail to customers with unpaid invoices
 */

$sapi_type = php_sapi_name();
$script_file = basename(__FILE__);
$path=dirname(__FILE__).'/';


// Test si mode batch
$sapi_type = php_sapi_name();
if (substr($sapi_type, 0, 3) == 'cgi') {
    echo "Error: You are using PHP for CGI. To execute ".$script_file." from command line, you must use PHP for CLI mode.\n";
	exit(-1);
}

$mode="confirm";
$invoiceref=trim($argv[1]);
$targetmail=trim($argv[2]);
$global_fichierPDF = trim($argv[3]);
$global_sendtoCC = $targetmail;
$global_userLoginDolibarr = "";
$global_paymentURI = "";

if($invoiceref == "" || $targetmail == "" || $global_fichierPDF == "") {
  print "\n\n\n";
  print "Erreur de lancement, veuillez lancer le script shell qui fait appel automatiquement à ce script php\n";
  print "  reference = $invoiceref\n";
  print "  targetmail = $targetmail\n";
  print "  fichier PDF = $global_fichierPDF\n";
  print "  send to cc  = $global_sendtoCC\n";
  print "\n\n\n";
  exit -2;
}

require($path."../../htdocs/master.inc.php");
require_once (DOL_DOCUMENT_ROOT."/core/class/CMailFile.class.php");
require_once DOL_DOCUMENT_ROOT.'/core/lib/payments.lib.php';

$langs->load('main');

// Global variables
$version=DOL_VERSION;
$error=0;



/*
 * Main
 */

@set_time_limit(0);
print "***** ".$script_file." (".$version.") pid=".dol_getmypid()." *****\n";
dol_syslog($script_file." launched with arg ".join(',',$argv));

$now=dol_now('tzserver');
$duration_value='none';
$duration_value2='none';

$error = 0;
print $script_file." launched with mode ".$mode." default lang=".$langs->defaultlang.(is_numeric($duration_value)?" delay=".$duration_value:"").(is_numeric($duration_value2)?" after=".$duration_value2:"")."\n";

if ($mode != 'confirm') $conf->global->MAIN_DISABLE_ALL_MAILS=1;

$sql = "SELECT f.ref, f.total_ttc, f.date_lim_reglement as due_date,";
$sql.= " s.rowid as sid, s.nom as name, s.email, s.default_lang";
//erics
$sql.= ",u.login";
if ($targettype == 'contacts') $sql.= ", sp.rowid as cid, sp.firstname as cfirstname, sp.lastname as clastname, sp.email as cemail";
$sql.= " FROM ".MAIN_DB_PREFIX."facture as f, ".MAIN_DB_PREFIX."societe as s";
//erics
$sql.= ", ".MAIN_DB_PREFIX."user as u";
if ($targettype == 'contacts') $sql.= ", ".MAIN_DB_PREFIX."socpeople as sp";
$sql.= " WHERE f.fk_statut = 1 AND f.paye = 0";
$sql.= " AND f.fk_soc = s.rowid";
//erics
$sql.= " AND u.fk_soc = s.rowid";
$sql.= " AND f.ref='" . $invoiceref ."'";
if (is_numeric($duration_value2)) $sql.= " AND f.date_lim_reglement >= '".$db->idate(dol_time_plus_duree($now, $duration_value2, "d"))."'";
if (is_numeric($duration_value)) $sql.= " AND f.date_lim_reglement < '".$db->idate(dol_time_plus_duree($now, $duration_value, "d"))."'";
if ($targettype == 'contacts') $sql.= " AND s.rowid = sp.fk_soc";
$sql.= " ORDER BY";
if ($targettype == 'contacts') $sql.= " sp.email, sp.rowid,";
$sql.= " s.email ASC, s.rowid ASC, f.ref ASC";	// Order by email to allow one message per email

//print $sql;
//exit;
$resql=$db->query($sql);
if ($resql)
  {
    $num = $db->num_rows($resql);
    $i = 0;
    $oldemail = 'none'; $oldsid = 0; $oldcid = 0; $oldlang='';
    $total = 0; $foundtoprocess = 0;
    $trackthirdpartiessent = array();

    print "We found ".$num." couples (unpayed validated invoices-".$targettype.") qualified\n";
    dol_syslog("We found ".$num." couples (unpayed validated invoices-".$targettype.") qualified");
    $message='';
    
    if ($num)
      {
        while ($i < $num)
	  {
            $obj = $db->fetch_object($resql);

	    //erics
	    $global_userLoginDolibarr=$obj->login;
	    $global_paymentURI=getOnlinePaymentUrl(0,'invoice',$invoiceref);
  
            $newemail=empty($obj->cemail)?$obj->email:$obj->cemail;
	    
            // Check if this record is a break after previous one
            $startbreak=false;
	    if ($newemail <> $oldemail || $oldemail == 'none') $startbreak=true;
	    if ($obj->sid && $obj->sid <> $oldsid) $startbreak=true;
	    if ($obj->cid && $obj->cid <> $oldcid) $startbreak=true;
	    
	    if ($startbreak)
	      {
                // Break onto sales representative (new email or cid)
                if (dol_strlen($oldemail) && $oldemail != 'none' && empty($trackthirdpartiessent[$oldsid.'|'.$oldemail]))
		  {
		    envoi_mail($mode,$oldemail,$message,$total,$oldlang,$oldtarget);
		    $trackthirdpartiessent[$oldsid.'|'.$oldemail]='contact id '.$oldcid;
		  }
                else
		  {
		    if ($oldemail != 'none')
		      {
			if (empty($trackthirdpartiessent[$oldsid.'|'.$oldemail])) print "- No email sent for '".$oldtarget."', total: ".$total."\n";
			else print "- No email sent for '".$oldtarget."', total: ".$total." (already sent to ".$trackthirdpartiessent[$oldsid.'|'.$oldemail].")\n";
		      }
		  }
                $oldemail = $newemail;
                $oldsid  = $obj->sid;
                $oldcid  = $obj->cid;
                $oldlang = $obj->lang;
                $oldtarget=(empty($obj->cfirstname) && empty($obj->clastname))?$obj->name:($obj->clastname." ".$obj->cfirstname);
                $message = '';
                $total   = 0;
                $foundtoprocess = 0;
                $target=(empty($obj->cfirstname) && empty($obj->clastname))?$obj->name:($obj->clastname." ".$obj->cfirstname);
                //if (empty($newemail)) print "Warning: Customer ".$target." has no email. Notice disabled.\n";
	      }
	    
            // Define line content
            $outputlangs=new Translate('',$conf);
            $outputlangs->setDefaultLang(empty($obj->default_lang)?$langs->defaultlang:$obj->default_lang);	// By default language of customer
            $outputlangs->load("bills");
            $outputlangs->load("main");
	    
            if (dol_strlen($newemail))
	      {
            	$message .= $outputlangs->trans("Invoice")." ".$obj->ref." : ".price($obj->total_ttc,0,$outputlangs,0,0,-1,$conf->currency)."\n";
            	dol_syslog("email_unpaid_invoices_to_customers.php: ".$newemail." ".$message);
            	$foundtoprocess++;
	      }
            print "Unpaid invoice ".$obj->ref.", price ".price2num($obj->total_ttc).", due date ".dol_print_date($db->jdate($obj->due_date),'day').", customer id ".$obj->sid." ".$obj->name.", ".($obj->cid?"contact id ".$obj->cid." ".$obj->clastname." ".$obj->cfirstname.", ":"")."email ".$newemail.", lang ".$outputlangs->defaultlang.": ";
            if (dol_strlen($newemail)) print "qualified.";
            else print "disqualified (no email).";
            print "\n";
	    
            unset($outputlangs);
	    
            $total += $obj->total_ttc;
	    
            $i++;
	  }
	
        // Si il reste des envois en buffer
        if ($foundtoprocess)
	  {
            if (dol_strlen($oldemail) && $oldemail != 'none' && empty($trackthirdpartiessent[$oldsid.'|'.$oldemail]))	// Break onto email (new email)
	      {
		envoi_mail($mode,$oldemail,$message,$total,$oldlang,$oldtarget);
		$trackthirdpartiessent[$oldsid.'|'.$oldemail]='contact id '.$oldcid;
	      }
            else
	      {
            	if ($oldemail != 'none')
		  {
		    if (empty($trackthirdpartiessent[$oldsid.'|'.$oldemail])) print "- No email sent for '".$oldtarget."', total: ".$total."\n";
		    else print "- No email sent for '".$oldtarget."', total: ".$total." (already sent to ".$trackthirdpartiessent[$oldsid.'|'.$oldemail].")\n";
		  }
	      }
	  }
      }
    else
      {
        print "No unpaid invoices found\n";
      }
    
    exit(0);
  }
 else
   {
     dol_print_error($db);
     dol_syslog("email_unpaid_invoices_to_customers.php: Error");
     
     exit(-1);
   }


/**
 * 	Send email
 *
 * 	@param	string	$mode			Mode (test | confirm)
 *  @param	string	$oldemail		Target email
 * 	@param	string	$message		Message to send
 * 	@param	string	$total			Total amount of unpayed invoices
 *  @param	string	$userlang		Code lang to use for email output.
 *  @param	string	$oldtarget		Target name
 * 	@return	int						<0 if KO, >0 if OK
 */
function envoi_mail($mode,$oldemail,$message,$total,$userlang,$oldtarget)
{
    global $conf,$langs;
    //erics
    global $global_sendtoCC;
    global $global_fichierPDF;
    global $invoiceref;
    global $global_userLoginDolibarr;
    global $global_paymentURI;

    if (getenv('DOL_FORCE_EMAIL_TO')) $oldemail=getenv('DOL_FORCE_EMAIL_TO');

    $newlangs=new Translate('',$conf);
    $newlangs->setDefaultLang(empty($userlang)?(empty($conf->global->MAIN_LANG_DEFAULT)?'auto':$conf->global->MAIN_LANG_DEFAULT):$userlang);
    $newlangs->load("main");
    $newlangs->load("bills");

    //erics custom soo2
    $subject = "[sud-ouest2.org] : Nouvelle Facture - $invoiceref...";
    $sendto = $oldemail;
    $from = $conf->global->MAIN_MAIL_EMAIL_FROM;
    $errorsto = $conf->global->MAIN_MAIL_ERRORS_TO;
    $msgishtml = 1;

    print "- Send email to '".$oldtarget."' (".$oldemail."), total: ".$total."\n";
    dol_syslog("email_unpaid_invoices_to_customers.php: send mail to ".$oldemail);

    $usehtml=1;
    if (dol_textishtml($conf->global->SCRIPT_EMAIL_UNPAID_INVOICES_CUSTOMERS_FOOTER)) $usehtml+=1;
    if (dol_textishtml($conf->global->SCRIPT_EMAIL_UNPAID_INVOICES_CUSTOMERS_HEADER)) $usehtml+=1;

    $allmessage='';
    if (! empty($conf->global->SCRIPT_EMAIL_UNPAID_INVOICES_CUSTOMERS_HEADER))
    {
    	$allmessage.=$conf->global->SCRIPT_EMAIL_UNPAID_INVOICES_CUSTOMERS_HEADER;
    }
    else
    {
    	$allmessage.= "Bonjour,".($usehtml?"<br>\n":"\n").($usehtml?"<br>\n":"\n");
    	$allmessage.= "une nouvelle facture a été générée par l'outil de gestion et de suivi comptable et administratif de l'association.".($usehtml?"<br>\n":"\n").($usehtml?"<br>\n":"\n");
    	$allmessage.= "Vous la trouverez attachée en pièce-jointe de ce courrier.".($usehtml?"<br>\n":"\n");
    	$allmessage.= "Sachez que les paiements par CB ou Paypal sont désormais gérés quasi automatiquement par notre plate-forme. En revanche, si vous payez par virement, par chèque ou autre, cela demande un délai de communication, et l'intervention d'un bénévole pour le pointage (trésorier) qui peut parfois prendre du temps.".($usehtml?"<br>\n":"\n");
	/*
	  $allmessage.= "En revanche, si vous payez par virement, par chèque ou autre, cela demande un délai de communication, et l'intervention d'un bénévole pour le pointage (trésorier) qui peut parfois prendre du temps." .($usehtml?"<br>\n":"\n");
	  $allmessage.= "Si c'est votre cas, cette relance a peut-être croisé votre paiement, merci alors de ne pas en tenir compte.".($usehtml?"<br>\n":"\n");
	*/
    	$allmessage.= ($usehtml?"<br>\n":"\n");
    	$allmessage.= "Vous pouvez vous connecter sur votre interface de suivi et de gestion administrative de votre compte à l'adresse suivante : <a href=\"" . $conf->global->MAIN_INFO_SOCIETE_WEB . "/mon-compte/\">" . $conf->global->MAIN_INFO_SOCIETE_WEB . "/mon-compte/</a>.".($usehtml?"<br>\n":"\n").($usehtml?"<br>\n":"\n");
	$allmessage.= "Votre identifiant de connexion est : <b>" . $global_userLoginDolibarr . "</b>" .($usehtml?"<br>\n":"\n").($usehtml?"<br>\n":"\n");
    	$allmessage.= ($usehtml?"<br>\n":"\n");
    	$allmessage.= "Merci de procéder au règlement de votre facture dès que possible.".($usehtml?"<br>\n":"\n").($usehtml?"<br>\n":"\n");
    	$allmessage.= ($usehtml?"<br>\n":"\n");

	//erics
	$allmessage.= ($usehtml?"<br>\n":"\n");
    	$allmessage.= "Vous pouvez payer votre facture de plusieurs manières:<ul><li>CB/Paypal via le lien suivant : <a href=\"" . $global_paymentURI . "\">paiement via paypal</a></li><li>ou par d'autres moyen de paiement : <a href=\"https://sud-ouest2.org/comment-regler-une-adhesion-faire-un-don-payer-un-service/\">https://sud-ouest2.org/comment-regler-une-adhesion-faire-un-don-payer-un-service/</a></li></ul>.".($usehtml?"<br>\n":"\n").($usehtml?"<br>\n":"\n");
    	$allmessage.= ($usehtml?"<br>\n":"\n");

	$allmessage.= ($usehtml?"<br>\n":"\n");
	$allmessage.= "--" . ($usehtml?"<br>\n":"\n");
    	$allmessage.= "Le trésorier" . ($usehtml?"<br>\n":"\n");
    	$allmessage.= "<a href=\"https://sud-ouest2.org/category/documentation/\">La documentation en ligne</a>" . ($usehtml?"<br>\n":"\n");	
    	$allmessage.= "<a href=\"https://sud-ouest2.org/support/\">Demander de l'aide</a>" . ($usehtml?"<br>\n":"\n");	
    }
    
    /*  //erics
	$allmessage.= $message.($usehtml?"<br>\n":"\n");
	$allmessage.= $langs->trans("Total")." = ".price($total,0,$userlang,0,0,-1,$conf->currency).($usehtml?"<br>\n":"\n");
    */
    
    if (! empty($conf->global->SCRIPT_EMAIL_UNPAID_INVOICES_CUSTOMERS_FOOTER))
    {
    	$allmessage.=$conf->global->SCRIPT_EMAIL_UNPAID_INVOICES_CUSTOMERS_FOOTER;
    	if (dol_textishtml($conf->global->SCRIPT_EMAIL_UNPAID_INVOICES_CUSTOMERS_FOOTER)) $usehtml+=1;
    }

    $mail = new CMailFile(
        $subject,
        $sendto,
        $from,
        $allmessage,
        array($global_fichierPDF),
        array(),
        array(),
        $global_sendtoCC,
        '',
        0,
        $msgishtml
    );

    $mail->errors_to = $errorsto;

    // Send or not email
    if ($mode == 'confirm')
    {
    	$result=$mail->sendfile();
    	if (! $result)
    	{
    		print "Error sending email ".$mail->error."\n";
    		dol_syslog("Error sending email ".$mail->error."\n");
    	}
    }
    else
    {
    	print "No email sent (test mode)\n";
    	dol_syslog("No email sent (test mode)");
    	$mail->dump_mail();
    	$result=1;
    }

    unset($newlangs);
    if ($result)
    {
        return 1;
    }
    else
    {
        return -1;
    }
}

