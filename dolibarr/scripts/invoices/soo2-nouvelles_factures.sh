#!/bin/bash
#outil d'envoi automatique des factures

LADATE=`date +%Y-%m-%d`
DOLIHOME="/var/www/dolibarr"
export LC_ALL=C
LISTING=`tempfile -p newfact`
rm -f ${LISTING}
#LISTING="/tmp/filexfEgCcTEST"
HOMESCRIPT="$(dirname "$0")"
cd "${HOMESCRIPT}"

function lecho() {
  echo "[soo2/facture] $1"
}

lecho "lancement de ${HOMESCRIPT} : ${0}"
lecho "  verification d'usage avant tout ($LADATE)..."

#on fait une petite verif
php ${DOLIHOME}/scripts/eric/soo2-corrige_affectation_user_societe.php

lecho "  generation des PDF des factures du jour dans ${LISTING}..."
#on genere tous les PDF des nouvelles facture (du jour)
${DOLIHOME}/scripts/invoices/soo2-rebuild_all_invoices.php 0 1 | grep "Build PDF for invoice" | sed s/"Build PDF for invoice \(.*\) - Lang = fr_FR"/"\1"/ > ${LISTING}

sleep 10

for facture in `cat ${LISTING}`
do
    lecho "  il faut envoyer la facture ${facture} ..."
    FICFAC="${DOLIHOME}/documents/facture/${facture}/${facture}.pdf"
    if [ -f ${FICFAC} ]; then
	maildest=`pdfgrep -P ".*boite.* (.*@.*) .*" ${FICFAC} | sed  s/".* \(.*@.*\)0%.*"/"\1"/g | /usr/local/bin/email_extract.pl`
	if [ -z "${maildest}" ]; then
	    maildest=`${DOLIHOME}/scripts/invoices/soo2-find_email_associated_to_invoice.php ${facture}`
	fi

	if [ ! -z "${maildest}" ]; then
	    lecho "  il faut envoyer la facture ${FICFAC} a ${maildest} ..."
	    #(
	    cd ${DOLIHOME}/documents/facture/${facture}
	    lecho "    changement de dossier pour ${PWD}"

		#${DOLIHOME}/scripts/invoices/soo2-email_nouvelle_facture.php ${facture} "eric.seigne@cap-rel.fr" ${facture}.pdf
		lecho "  on lance -> ${DOLIHOME}/scripts/invoices/soo2-email_nouvelle_facture.php ${facture} ${maildest} ${facture}.pdf"
		${DOLIHOME}/scripts/invoices/soo2-email_nouvelle_facture.php ${facture} ${maildest} ${facture}.pdf
		lecho "  sleep 3..."
		sleep 3
	    #)
	fi
    else
	lecho "  Erreur grave, le fichier ${FICFAC} n'est pas présent !!!!"
    fi
done

chown www-data:www-data ${DOLIHOME}/documents/facture -R

exit 0
