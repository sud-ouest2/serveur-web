#!/usr/bin/env php
<?php
/*
 * Copyright (C) 2005		Rodolphe Quiedeville <rodolphe@quiedeville.org>
 * Copyright (C) 2005-2013	Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2013		Juanjo Menent <jmenent@2byte.es>
 * Copyright (C) 2017-2018	Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

$sapi_type = php_sapi_name();
$script_file = basename(__FILE__);
$path=dirname(__FILE__).'/';
$mode = "confirm";

// Test si mode batch
$sapi_type = php_sapi_name();
if (substr($sapi_type, 0, 3) == 'cgi') {
    echo "Error: You are using PHP for CGI. To execute ".$script_file." from command line, you must use PHP for CLI mode.\n";
	exit(-1);
}

require($path."../../htdocs/master.inc.php");
require_once (DOL_DOCUMENT_ROOT."/core/class/CMailFile.class.php");

$langs->load('main');

// Global variables
$version=DOL_VERSION;
$error=0;


/*
 * Main
 */

@set_time_limit(0);
//print "***** ".$script_file." (".$version.") pid=".dol_getmypid()." *****\n";
dol_syslog($script_file." launched with arg ".join(',',$argv));

$now=dol_now('tzserver');
$duration_value='none';
$duration_value2='none';

$error = 0;
//print $script_file." launched with mode ".$mode." default lang=".$langs->defaultlang.(is_numeric($duration_value)?" delay=".$duration_value:"").(is_numeric($duration_value2)?" after=".$duration_value2:"")."\n";

if ($mode != 'confirm') $conf->global->MAIN_DISABLE_ALL_MAILS=1;

$sql = "SELECT mailsoo as m1,email as m2,mailsecours as m3 from llx_adherent as a INNER JOIN llx_adherent_extrafields as ae ON a.rowid=ae.fk_object WHERE fk_soc IS NOT NULL AND statut = 1";

//print $sql;
//exit;
$resql=$db->query($sql);
if ($resql) {
  $num = $db->num_rows($resql);
  //print "We found ".$num." adherents\n";
  //dol_syslog("We found ".$num." accounts in error");
  $message='';
  for($i = 0; $i < $num; $i++) {
    $obj = $db->fetch_object($resql);
    $m = trim($obj->m1);
    if($m == "")
      $m = trim($obj->m2);
    if($m == "")
      $m = trim($obj->m3);
    
    if($m != "")
      echo $m . "\n";
  }
 }
